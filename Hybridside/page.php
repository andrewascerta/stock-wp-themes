<?php get_header(); ?>
<!--Reference to the header | DON'T TOUCH THIS | -->
<?php get_sidebar(); ?>
<!--Reference to the Sidebar | DON'T TOUCH THIS | -->

<!--Content Start -------------------------------------------------------------------------------------------------------------------------->
<div id="content" >
								
								
								<?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                       
                       
                       
                       
                        <div class="post single"><!--Post Start-->


                                          
                                          <div class="info_index"><!-- Post Time Info , Category and Comments Start-->
                                             <ul>
                                                                                             
                                                <li class="time">
                                                <?php the_time('j') ?>
                                                <?php the_time(' F ') ?>
                                                <?php the_time(' Y ') ?>
                                                </li>
                                                         
                                                                                                
                                                <!-- Facebook Like Button -->
                                                <li class="social">
                                                <img class="icon3" src="<?php bloginfo('template_directory'); ?>/images/facebook_16.png" alt="Follow Me!" width="12" height="12" /> 
                                                <iframe src="http://www.facebook.com/plugins/like.php?href=<?php echo urlencode(get_permalink($post->ID)); ?>& 						              layout=button_count&amp&show_faces=false&amp&width=150&amp&action=like&amp&colorscheme=light&amp&height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:21px;  " 	allowTransparency="true">
                                                </iframe>
                                                </li>
                                                
                                                <!-- Twitter Button -->
                                                <li class="social">
                                                <img class="icon3" src="<?php bloginfo('template_directory'); ?>/images/twitter_16.png" alt="Follow Me!" width="12" height="12" />
                                                <script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
                                                <a  title="<?php the_title(); ?>" href="http://twitter.com/share?url=<?php echo urlencode(get_permalink($post->ID)); ?>" data-count="horizontal" class="twitter-share-button"></a>
                                                </li>
                        
                                                <li class="author" > 
                                                <?php _e('Posted By', 'theme_textdomain') ; ?>
                                                <?php the_author_link(); ?>
                                                </li>
                     									
																<?php
                                                if ( has_post_thumbnail() ) { ?>
                                                <div class="info_thumb" >
                                                <?php the_post_thumbnail( 'first' ); ?>
                                                </div>
                                                <?php } else {
                                                // the current post lacks a thumbnail
                                                }
                                                ?> 
                     
                                             </ul>
                                          </div> <!-- Post Time Info , Category, Tags End-->
                                          
                                          
                                          
                                          <div class="title"><!-- Title Start-->
                                                <h1 > 
                                                <a  href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
                                                <?php the_title(); ?>
                                                </a>
                                                </h1>
                                                 
                                                <h6 class="print_time"><!-- Only for Print.css-->
                                                <?php the_time('j') ?>
                                                <?php the_time(' F ') ?>
                                                <?php the_time(' Y ') ?>
                                                Posted By
                                                <?php the_author_link(); ?>
                                                </h6>
                                             
                                          </div><!-- Title End-->
                                                          
                                                          
                                                          
                                          <!-- Text Start-->
                                          <div class="text_content">
                                                <?php the_content(''); ?>
                                          </div>
                                          <!-- Text End-->
                        
                        
                                       
                        </div>
                        <!--Single Post End -->





                        
                        
                        <?php endwhile; else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.', 'theme_textdomain') ; ?></p>
                        <?php endif; ?>


</div>
<!--Content End ---------------------------------------------------------------------------------------------------------------------------->

<!--Reference to the Footer | DON'T TOUCH THIS | -->
<?php get_footer(); ?>