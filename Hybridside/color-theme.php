<?php $option_css="" ?>
<?php if (get_option('pov_custom_css')) { $option_css = get_option('pov_custom_css') ; } ?>

<style>
a, a:link, a:focus, a:active,a:visited{color:<?php echo $option_css; ?>;}
#wrap li.current-post-ancestor, #wrap li.current_page_item, #wrap li.current-cat , #wrap li.current-menu-item  { background:<?php echo $option_css; ?>; }
.navigation li a:hover {background:<?php echo $option_css; ?>; }
.navigation li.current-post-ancestor, .navigation li.current_page_item, .navigation li.current-cat, .navigation li.current-menu-item { background:<?php echo $option_css; ?>; }
.post a.more{background:<?php echo $option_css; ?>;}
.post h1 a:hover {color:<?php echo $option_css; ?>;}
.info_index li.sticky{background-color:<?php echo $option_css; ?>;  }
.prevleft a:hover, .nextright a:hover { color:<?php echo $option_css; ?>;}
.prevpostleft a, .nextpostright a{ color: <?php echo $option_css; ?>; }
.currentbrowsing h1{ color:<?php echo $option_css; ?>; }
ol.commentlist a, ol.commentlist a:link, ol.commentlist a:active, ol.commentlist a:visited{color:<?php echo $option_css; ?>;}
#wp-calendar td a, #wp-calendar td a:link, #wp-calendar td a:active, #wp-calendar td a:focus {color:<?php echo $option_css; ?>;}
</style>