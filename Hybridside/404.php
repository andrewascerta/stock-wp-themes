<?php get_header(); ?>
<!--Reference to the header | DON'T TOUCH THIS | -->
<?php get_sidebar(); ?>
<!--Reference to the Sidebar | DON'T TOUCH THIS | -->

<!--Content Start -------------------------------------------------------------------------------------------------------------------------->
<div id="content" >
    <div id="currentbrowsing">
		<h1><?php _e('Wrong Page ', 'theme_textdomain') ; ?></h1>
      <p><?php _e('Whatever you are looking for, it could not be found. Please try searching again or maybe browse through our index page. ', 'theme_textdomain') ; ?></p>
    </div>
</div>
<!--Content End ---------------------------------------------------------------------------------------------------------------------------->

<!--Reference to the Footer | DON'T TOUCH THIS | -->
<?php get_footer(); ?>