<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php language_attributes(); ?> />
<html xmlns="http://www.w3.org/1999/xhtml"  xml:lang="en" lang="en"><head profile="http://gmpg.org/xfn/11">
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="description" content=" <?php if (get_option('pov_seodescription')) { $seo_description = get_option('pov_seodescription') ; } ?>  <?php echo $seo_description; ?>">
<meta name="author" content=" <?php if (get_option('pov_seoname')) { $seo_author = get_option('pov_seoname') ; } ?>  <?php echo $seo_author; ?>" />
<meta name="contact" content="<?php if (get_option('pov_seocontact')) { $seo_contact = get_option('pov_seocontact') ; } ?>  <?php echo $seo_contact; ?>" />
<meta name="copyright" content="<?php if (get_option('pov_seocopyright')) { $seo_copy = get_option('pov_seocopyright') ; } ?>  <?php echo $seo_copy ; ?>" />
<meta name="keywords" content="<?php if (get_option('pov_seokeywords')) { $seo_keywords = get_option('pov_seokeywords') ; } ?>  <?php echo  $seo_keywords ; ?>" />
<meta name="google-site-verification" content="<?php if (get_option('pov_seogooglesite')) { $seo_googlesite = get_option('pov_seogooglesite') ; } ?>  <?php echo  $seo_googlesite ; ?>"><!--  Google http://google.com/webmasters -->
	
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
<?php if (is_search()) { ?>
<meta name="robots" content="noindex, nofollow" /> 
<?php } ?>

<title>

		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
</title>
	
<meta name="title" content="<?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>">



<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="shortcut icon" href="<?php echo home_url() ?>/favicon.ico" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>


<!-- Don't touch this! 
<?php global $options;
foreach ($options as $value) {
if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); } }
?>



<!-- Stylesheet link -->
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css" media="screen" />
<!-- <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style_rtl.css" type="text/css" media="screen" /> -->

<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/print.css" type="text/css" media="only print" />


<!--Include Subfooter Start ---------------------------------------------------------------------------------------------------------------->
<?php include(TEMPLATEPATH."/color-theme.php");?>
<!--Include Subfooter End ------------------------------------------------------------------------------------------------------------------>

<?php wp_head(); ?>

</head>

<!--Body Start ----------------------------------------------------------------------------------------------------------------------------->
<body class="<?php body_class() ?>" >
<!--[if IE]>
<div class="alert">You're running an older and buggy version browser: For your safety update to a modern web browser.</div>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/ie7.css" type="text/css" media="screen" />
<![endif]-->

<!--Container Start ------------------------------------------------------------------------------------------------------------------------>
<div id="container">




<!--Header Start --------------------------------------------------------------------------------------------------------------------------->
<!--Blog Description Text and Social Networks Links-->
<div id="header" >


                               
                               <div class="logo"> <!--Logo Start-->
                               
                               <!-- Don't Touch This -->
                               <?php global $options;
                               foreach ($options as $value) {
                               if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
                               }?>
                                          
                                          <h2>
                                                  <a href="<?php echo get_option('home'); ?>/">
                                                  <?php if($pov_logo) { ?>
                                                  <img src="<?php echo $pov_logo;?>" alt="Go Home"/>
                                                  <?php } else { bloginfo('name'); } ?>
                                                  </a>
                                          </h2>
                                          <h6>
                                                  <?php if($pov_logo) { ?>
                                                  <?php } else { bloginfo('description'); } ?>
                                          </h6>
                                  </div><!--Logo End-->
                                  
                                  
                                  
                                  <div id="me_text"> <!--Description and links Start-->
                                  
                                                  <!--Slogan Description-->
                                                  <?php if ($pov_dislogan== "true") { } else { ?>
                                                  <h6 class="description">           
                                                  <?php if (get_option('pov_slogan')) { $slogan_header = get_option('pov_slogan') ; } ?>
                                                  <?php echo $slogan_header; ?>
                                                  </h6>  
                                                  <?php } ?>  
                                                  <!--Slogan Description End-->
                                                  
                                                  <?php if ($pov_dis_social== "true") { } else { ?>    
                                                  <ul>
                                                                                              
                  
                                                 <!--Feedburner -->
                                                 <?php $feedburner_account="#" ?><?php if (get_settings('pov_feedburner_account')) { $feedburner_account = get_settings('pov_feedburner_account') ; } ?>
                                                 <li class="nobullet">
                                                 <img class="icon" src="<?php bloginfo('template_directory'); ?>/images/rss_16.png" alt="Subscribe!" width="12" height="12" />
                                                 <?php if ($pov_dis_feedburner== "true") { ?>                                                  
																 <a href="<?php bloginfo('rss2_url'); ?>" > <?php _e('RSS Feed 2.0' , 'theme_textdomain'); ?> </a> 
																 <?php } else { ?>  
                                                 <a href="http://feeds.feedburner.com/<?php echo $feedburner_account ?>" > <?php _e('Subscribe' , 'theme_textdomain'); ?>   </a>
                                                 <?php } ?>  
                                                 </li>
                                                 
                                                 <!--Twitter -->
                                                 <?php $twit_user_name="#" ?><?php if (get_settings('pov_twitter_user_name')) { $twit_user_name = get_settings('pov_twitter_user_name') ; } ?>
                                                 <li class="nobullet">
                                                 <img class="icon" src="<?php bloginfo('template_directory'); ?>/images/twitter_16.png" alt="Follow Me!" width="12" height="12" />
                                                 <a href="http://twitter.com/<?php echo $twit_user_name;  ?>"  > <?php _e('Follow on Twitter', 'theme_textdomain') ; ?>   </a>
                                                 </li>
                                                 
                                                 <!--Facebook -->
                                                 <?php $fac_user_name="#" ?><?php if (get_settings('pov_facebook_user_name')) { $fac_user_name = get_settings('pov_facebook_user_name') ; } ?>
                                                 <li class="nobullet">
                                                 <img class="icon" src="<?php bloginfo('template_directory'); ?>/images/facebook_16.png" alt="Add Me!" width="12" height="12" />
                                                 <a href="http://facebook.com/<?php echo $twit_user_name;?>"> <?php _e('Add on Facebook ', 'theme_textdomain') ; ?>  </a>
                                                 </li>
                  
                                                 </ul>
                                                 <?php } ?>  
                              </div><!--Description and links End-->
                                                 
</div>                             
<!--Header End ----------------------------------------------------------------------------------------------------------------------------->



<!--Menu Start ----------------------------------------------------------------------------------------------------------------------------->
<div id="wrap_container" >
                              
                              <div class="search" ><!--Search Start -->
                              <?php include (TEMPLATEPATH . '/searchform.php'); ?>
                              </div> <!--Search End -->
                              
                            	<!--Menus navigation for wordpress 3.0 support -->

                              <?php wp_nav_menu(array( 'fallback_cb' => 'display_categories' , 'menu' => 'primary-menu', 'container_class' => 'main-menu', 'container_id' => 'wrap', 'theme_location' => 'primary-menu' ) ); ?>

</div>
<!--Menu End ------------------------------------------------------------------------------------------------------------------------------->


<!--Main Start ----------------------------------------------------------------------------------------------------------------------------->
<!--Content + Sidebar -->
<div id="main">