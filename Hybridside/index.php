<?php get_header(); ?>
<!--Reference to the header | DON'T TOUCH THIS | -->
<?php get_sidebar(); ?>
<!--Reference to the Sidebar | DON'T TOUCH THIS | -->

<!--Content Start -------------------------------------------------------------------------------------------------------------------------->
<div id="content" >

								<?php $post = $posts[0]; $c=0; ?>
                        <?php if (have_posts()) : ?><!-- The Loop Start | DON'T TOUCH THIS | -->
                        <?php while (have_posts()) : the_post(); ?>


                        
								<div class="post"><!--Post Start-->


                                          
                                          <div class="info_index"><!-- Post Time Info , Category and Comments Start-->
                                             <ul>
																<?php if (is_sticky()) {?>  <!-- If there is a sticky post, a badge will be showed on the left-->
                                                <li class="sticky"><?php _e('In Evidence', 'theme_textdomain') ; ?> </li>
                                                <?php } else { ?>
                                                <!-- Do Nothing-->
                                                <?php } ?>
                                                
                                                <li class="time">
                                                <?php the_time('j') ?>
                                                <?php the_time(' F ') ?>
                                                <?php the_time(' Y ') ?>
                                                </li>
                                                
                                                <li class="category"> 
                                                <?php the_category(', ') ?>
                                                </li>
                                                
                                                <li class="comments" >          
                                                <?php comments_popup_link('0 Comments ', '1 Comments', '%  Comments'); ?>
                                                </li>
                                                                                                
                                                <!-- Facebook Like Button -->
                                                <li class="social">
                                                <img class="icon3" src="<?php bloginfo('template_directory'); ?>/images/facebook_16.png" alt="Follow Me!" width="12" height="12" /> 
                                                <iframe src="http://www.facebook.com/plugins/like.php?href=<?php echo urlencode(get_permalink($post->ID)); ?>& 						              layout=button_count&amp&show_faces=false&amp&width=150&amp&action=like&amp&colorscheme=light&amp&height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:21px;  " 	allowTransparency="true">
                                                </iframe>
                                                </li>
                                                
                                                <!-- Twitter Button -->
                                                <li class="social">
                                                <img class="icon3" src="<?php bloginfo('template_directory'); ?>/images/twitter_16.png" alt="Follow Me!" width="12" height="12" />
                                                <script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
                                                <a  title="<?php the_title(); ?>" href="http://twitter.com/share?url=<?php echo urlencode(get_permalink($post->ID)); ?>" data-count="horizontal" class="twitter-share-button"></a>
                                                </li>
                        
                                                <li class="author" > 
                                                <?php _e('Posted By', 'theme_textdomain') ; ?>
                                                <?php the_author_link(); ?>
                                                </li>
                     
                     
                                             </ul>
                                          </div> <!-- Post Time Info , Category, Tags End-->




														<?php
                                          if ( has_post_thumbnail() ) { ?><!-- If there is a Thumbnail, it will be showed -->
                                                                  <div class="post-thumb" >
                                                                  <?php the_post_thumbnail( 'first' ); ?>
                                                                  </div>
                                                                  
                                                                  <div class="title"><!-- Title Start-->
                                                                     <h1 > 
                                                                     <a  href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
                                                                     <?php the_title(); ?>
                                                                     </a>
                                                                     </h1> 
                                                                  </div><!-- Title End-->
                                           <?php } else { ?>  <!-- If there isn't a Thumbnail, it will not be showed and the title doesn't require a min-height > class="title" -->
                                                                  <div ><!-- Title Start-->
                                                                     <h1 > 
                                                                     <a  href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
                                                                     <?php the_title(); ?>
                                                                     </a>
                                                                     </h1> 
                                                                  </div><!-- Title End-->                  
                                           <?php }?>                      

                                                                  
                                                                  
                                                                  <div class="post-text"><!-- Text Start-->
                                                                  <?php if ( has_excerpt () ) { ?><!-- If Manual Excerpt is used, it will be showed, else will be showed the content -->
																								<?php the_excerpt(); ?>
                                                                        <div class="read" ><a class="more" href="<?php the_permalink() ?>"> <?php _e('Read More', 'theme_textdomain') ; ?> </a>
                                                                        <?php the_tags('<ul class="tags"><li>','</li><li>','</li></ul>'); ?>
                                                                        </div>
                                                                  <?php } else { ?>
																								<?php the_content(''); ?>
                                                                        	<!-- If the !more is used, the content will be cutted by the position of the more inside the post, else will be showed the full content -->
																									<?php if ($pos=strpos($post->post_content, '<!--more-->')): ?>
                                                                           <div class="read" ><a class="more" href="<?php the_permalink() ?>#more-<?php the_id() ?>"> <?php _e('Read More', 'theme_textdomain') ; ?></a>
                                                                           <?php the_tags('<ul class="tags"><li>','</li><li>','</li></ul>'); ?>
                                                                           </div>
                                                                           <?php else : ?>
                                                                           <?php endif; ?>
                                                                  <?php } ?>

                                                                  </div><!-- Text End-->
                                                                  

								
								</div><!-- Post End-->

								<?php endwhile; else: ?> 
                        <?php endif; ?> <!-- The Loop END | DON'T TOUCH THIS | -->


</div>
<!--Content End ---------------------------------------------------------------------------------------------------------------------------->


<!--Navigation Post Link Start -->
<div id="navigation">
<div class="nextright"><?php previous_posts_link('New Entries &raquo;' )  ?></div>
<div class="prevleft"><?php next_posts_link(' &laquo; Older Entries' ) ?></div>
</div>
 <!--Navigation Post Link Start -->

<!--Reference to the Footer | DON'T TOUCH THIS | -->
<?php get_footer(); ?>