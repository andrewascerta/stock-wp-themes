<?php
// Localization Support
define('THEME_PATH', get_template_directory());
load_theme_textdomain( 'theme_textdomain', THEME_PATH . '/languages' );
$locale = get_locale();
$locale_file = THEME_PATH . "/languages/$locale.php";
if ( is_readable( $locale_file ) ) require_once( $locale_file );

// Create the Option panel for the theme inside the dashboard
require_once(TEMPLATEPATH . '/dashboard.php'); 

// Reference to javascript file for the theme
if (!is_admin()) add_action( 'wp_print_scripts', 'hybridside_javascript' );
function hybridside_javascript( ) {
wp_enqueue_script('hybridside_js', get_bloginfo('template_directory').'/javascript/hybridside_js.js', array( 'jquery' ) );
}

// Custom Background Function inside the Dashboard > Appereance > Background
add_custom_background();

// This feature enables post and comment RSS feed links to head
add_theme_support( 'automatic-feed-links' );

// Enable the custom navigation menus inside the Dashboard > Appereance > Menus
add_action( 'init', 'register_my_menu' );

function register_my_menu() {
	register_nav_menu( 'primary-menu', __( 'Primary Menu' ) );
}

// If menus is not used wordpress will replace it with the standard one
function display_pages() {
echo '<div class="navigation">';
wp_list_pages('title_li=&depth=1&number=5');
echo 
'</div>';
}

function display_categories() {
echo '<div class="navigation">';
wp_list_categories('title_li=&depth=1&number=5');
echo '
</div>';
}

// Enable Thumbnail support for each post
add_theme_support( 'post-thumbnails' );
add_image_size('first', 150, 150, true);
add_image_size('second', 90, 90, true);

// Options for modify the manual excerpt
function new_excerpt_more($more) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Create the Widget Area for the theme
if ( function_exists('register_sidebar') )
	register_sidebar(array(
	'name'=>'widget1',// Sidebar 
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '<h6>  ',
	'after_title' => '</h6>',
));

register_sidebar(array('name'=>'widget2', // Subfooter 
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '<h6> ',
	'after_title' => '</h6>',
));

register_sidebar(array('name'=>'widget3', // Subfooter 
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '<h6>  ',
	'after_title' => '</h6>',
));

register_sidebar(array('name'=>'widget4', // Subfooter 
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '<h6>  ',
	'after_title' => '</h6>',
));

register_sidebar(array('name'=>'widget5', // Subfooter 
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '<h6> ',
	'after_title' => '</h6>',
));



// When you upload an image, if you select full size, wordpress will re-size it to the width of che content >>> 550px
$GLOBALS['content_width'] = 550; 


// Comment reply 

function mytheme_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
<div id="comment-<?php comment_ID(); ?>">
<div class="comment-author vcard">
<?php echo get_avatar($comment,$size='36',$default='<path_to_url>' ); ?>
 
<?php printf(__('<cite class="fn">%s</cite>  <span class="says">says:</span>'), get_comment_author_link()) ?>
</div>
<?php if ($comment->comment_approved == '0') : ?>
<em><?php _e('Your comment is awaiting moderation.') ?></em>
<br />
<?php endif; ?>
 
<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars(get_comment_link( $comment->comment_ID )) ?>">
<?php printf(__('%1$s at %2$s'), get_comment_date(),get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','') ?></div>
 
<?php comment_text() ?>
<?php if($args['max_depth']!=$depth) { ?>
<div class="reply">
<?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
</div>
<?php } ?>
</div>
<?php
}

// Author Bio in each single post
function get_author_bio ($content=''){
    global $post;

    $post_author_name=get_the_author_meta("display_name");
    $post_author_description=get_the_author_meta("description");
    $html="<div class='clearfix' id='about_author'>\n";
	 $html.="<h1>Author</h1>";
    $html.="<img width='80' height='80' class='avatar' src='http://www.gravatar.com/avatar.php?gravatar_id=".md5(get_the_author_email()). "&default=".urlencode($GLOBALS['defaultgravatar'])."&size=80&r=PG' alt='PG'/>\n";
    $html.="<div class='author_text'>\n";
    $html.="<h3>$post_author_name</h3>\n";
    $html.= "<p>$post_author_description.</p>\n";
    $html.="</div>\n";
    $html.="</div>\n";
    $html.="<div class='clear'></div>\n";
    $content .= $html;

    return $content;
}


// Extend the contact info with social networks links
function my_new_contactmethods( $contactmethods ) {
	// Add Twitter
	$contactmethods['twitter'] = 'Twitter';
	//add Facebook
	$contactmethods['facebook'] = 'Facebook';

return $contactmethods;
}
	add_filter('user_contactmethods','my_new_contactmethods',10,1);
?>