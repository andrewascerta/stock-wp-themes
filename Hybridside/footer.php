<?php global $options; 
foreach ($options as $value) {
if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); } }
?> <!--Don't Touch This-->

</div>
<!--Main End ------------------------------------------------------------------------------------------------------------------------------->


<!--Include Subfooter Start ---------------------------------------------------------------------------------------------------------------->
<?php if ($pov_disubfooter== "true") { } else { ?>
<?php include(TEMPLATEPATH."/sidebar-footer.php");?>
<?php } ?>
<!--Include Subfooter End ------------------------------------------------------------------------------------------------------------------>




<!--Footer Start --------------------------------------------------------------------------------------------------------------------------->
<div id="footer"  >

                        <h6 class="right">
                        
                           <img class="icon2" src="<?php bloginfo('template_directory'); ?>/images/wordpress_16.png" alt="Subscribe!" width="8" height="8" /> 
                           <a href="http://www.wordpress.org"> Wordpress </a> - <?php _e('Template:', 'theme_textdomain') ; ?> <a href="http://hybridside.llow.it">Hybrid</a> - 
                        
                        <!--Credit Link-->
									<?php if ($pov_discredit== "true") { } else { ?>
                           <?php _e('Design by:', 'theme_textdomain') ; ?> <a href="http://www.llow.it">llowit</a> 
                           <?php } ?> 
                        <!--Credit Link End-->          
                        
                        </h6>
                        
                        <!--Text License-->
                        <h6>
									<?php $footer_license="Insert Here your tipology of license" ?>
                           <?php if (get_option('pov_footer_license')) { $footer_license = get_option('pov_footer_license') ; } ?>
                           <a><?php echo date("Y"); ?>
                           <?php bloginfo('name'); ?>
                           </a> <?php echo $footer_license; ?>
                        </h6>            
                        <!--Text License End-->
                

								<ul>
                                <!--Feedburner -->
											<?php $feedburner_account="#" ?><?php if (get_settings('pov_feedburner_account')) { $feedburner_account = get_settings('pov_feedburner_account') ; } ?>
                                 <li class="nobullet">
                                 <img class="icon" src="<?php bloginfo('template_directory'); ?>/images/rss_16.png" alt="Subscribe!" width="12" height="12" />
                                 <?php if ($pov_dis_feedburner== "true") { ?>                                                  
                                 <a href="<?php bloginfo('rss2_url'); ?>"><?php _e('RSS Feed 2.0' , 'theme_textdomain'); ?></a> 
                                 <?php } else { ?>  
                                 <a href="http://feeds.feedburner.com/<?php echo $feedburner_account ?>"> <?php _e('Subscribe' , 'theme_textdomain'); ?>  </a>
                                 <?php } ?>  
                                 </li>
               
                               <!--Twitter Profile Link -->
                               <?php $twit_user_name="#" ?><?php if (get_settings('pov_twitter_user_name')) { $twit_user_name = get_settings('pov_twitter_user_name') ; } ?>
                               <li class="nobullet">
                               <img class="icon" src="<?php bloginfo('template_directory'); ?>/images/twitter_16.png" alt="Follow Me!" width="8" height="8" />
                               <a href="http://twitter.com/<?php echo $twit_user_name;  ?>"> <?php _e('Follow on Twitter', 'theme_textdomain') ; ?>  </a>
                               </li>
                               
                               <!--Facebook Link -->
                               <?php $fac_user_name="#" ?><?php if (get_settings('pov_facebook_user_name')) { $fac_user_name = get_settings('pov_facebook_user_name') ; } ?>
                               <li class="nobullet">
                               <img class="icon" src="<?php bloginfo('template_directory'); ?>/images/facebook_16.png" alt="Add Me!" width="8" height="8" />
                               <a href="http://facebook.com/<?php echo $twit_user_name;?>">  <?php _e('Add on Facebook ', 'theme_textdomain') ; ?>  </a>
                               </li>

								</ul>


</div> 
<!--Footer End ----------------------------------------------------------------------------------------------------------------------------->

</div>
<!--Container End -------------------------------------------------------------------------------------------------------------------------->

<div style="height:1px;"></div>


<!--Google Analytics From Dashboard-->
<?php 
$pov_google_analytics = get_option('pov_google_analytics');
if ($pov_google_analytics != '') { echo stripslashes($pov_google_analytics); }
?>

<!--Google Analytics From Dashboard End-->

<?php wp_footer(); ?>
<!-- Javascript Link -->
<?php wp_enqueue_script('jquery'); ?>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/javascript/hybridside_js.js"></script>
</body>
<!--Body End ------------------------------------------------------------------------------------------------------------------------------->
</html>