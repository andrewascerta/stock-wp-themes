<?php

$themename = "Hybridside";
$shortname = "pov";
$mx_categories_obj = get_categories('hide_empty=0');
$mx_categories = array();
foreach ($mx_categories_obj as $mx_cat) {
	$mx_categories[$mx_cat->cat_ID] = $mx_cat->cat_name;
}
$categories_tmp = array_unshift($mx_categories, "Select a category:");	
$trans = __("Logo Display", 'theme_textdomain');

$options = array (

array( "name" => $themename." Options",  
    "type" => "title"),  
  
array( "name" => __("General"),  
       "type" => "section"),  	 
array( "type" => "open"), 



array( 	"name" => $trans,
			"desc" => __("Insert the URL address of your logo(upload in your media gallery) Dimensions are 300px x 60px). If you leave the form empty will display your blog title and description"),
			"id" => $shortname."_logo",
			"type" => "text",
			"std" => "http://www.llow.it/wordpress/wp-content/uploads/2011/05/logo_hybridside-e1305454593759.png"),
					

array(  "name" => __("Slogan Description"),
					"desc" => __("Enter the text for your blog slogan."),
					"id" => $shortname."_slogan",
					"type" => "text",
					"std" => "This is a free space where you can enter the text for your blog slogan, a description or whatever you want to say to your readers."),	
					
array(	"name" => __("Disable Slogan Description"),
					"desc" => __("You can disable the slogan description on the header"),
					"id" => $shortname."_dislogan",
					"std" => "false",
					"type" => "checkbox"),
					
array( "type" => "close"),	




array( "name" => __("Color Scheme"),  
       "type" => "section"),  	 
array( "type" => "open"), 			
			
array(  "name" => __("Theme Color Scheme"),
        "type" => "heading",
			"desc" => __("Choose your color scheme."),
       ),


array(        "name" => __("Color"),   
    "desc" => __("Just replace the default color (#009DFF) with the one you want."),   
    "id" => $shortname."_custom_css",   
    "type" => "text",   
    "std" => "#009DFF"
),

array( "type" => "close"),				
										

array( "name" => __("Social Address Links"),  
       "type" => "section"),  	 
array( "type" => "open"), 			
			



array(  "name" => __("Your FeedBurner address"),
        	"desc" => __("Enter your Feedburner account name"),
        	"id" => $shortname."_feedburner_account",
        	"type" => "text",
        	"std" => ""),	

array(	"name" => __("I don't want to use Feedbuner, just use the simple RSS"),
					"desc" => __("If you don't have a Feedburner account and don't want to use it, just check this box and your user will see the simple RSS"),
					"id" => $shortname."_dis_feedburner",
					"std" => "false",
					"type" => "checkbox"),		
		
array(  "name" => __("Your Twitter account"),
        	"desc" => __("Enter your Twitter account name"),
        	"id" => $shortname."_twitter_user_name",
        	"type" => "text",
        	"std" => ""),
array(  "name" => __("Your Facebook account"),
        	"desc" => __("Enter your Facebook account name ex: aldo.rossi"),
        	"id" => $shortname."_facebook_user_name",
        	"type" => "text",
        	"std" => ""),
array(	"name" => __("Disable Social links"),
					"desc" => __("You can disable the social links on header"),
					"id" => $shortname."_dis_social",
					"std" => "false",
					"type" => "checkbox"),
			
array( "type" => "close"), 	


array(  "name" => __("Widget Area on Subfooter"),
            "type" => "heading",
			"desc" => __("Subfooter is under the main content.If you decide to active it you will have 4 widgets area more ."),
       ),	
			
array( "name" => __("Subfooter Widget Area"),  
       "type" => "section"),  	 
array( "type" => "open"), 	
			
array(	"name" => __("Disable Subfooter"),
					"desc" => __("Subfooter div and the widgets area will be turned off"),
					"id" => $shortname."_disubfooter",
					"std" => "false",
					"type" => "checkbox"),
array( "type" => "close"), 


	

array( "name" => __("Footer"),  
       "type" => "section"),  	 
array( "type" => "open"), 	
								
					
array(  "name" => __("Footer"),
         "type" => "heading",
			"desc" => __("License for your blog ."),
       ),
					

array(  "name" => __("License Text on Footer"),
					"desc" => __("Enter the text for your license in the footer"),
					"id" => $shortname."_footer_license",
					"type" => "text",
					"std" => "Enter the text for your license inside the footer"),
					

array(	"name" => __("Disable Credit Link"),
					"desc" => __("It's completely optional , but if you like the Theme i would appreciate it if you keep the credit link at the bottom"),
					"id" => $shortname."_discredit",
					"std" => "false",
					"type" => "checkbox"),
					
array( "type" => "close"), 					

					
	
					
array( "name" => __("Google Analytics"),  
       "type" => "section"),  	 
array( "type" => "open"), 		

	
		array(  "name" => __("Google Analytics"),
            "type" => "heading",
			"desc" => __("Please paste your Google Analytics (or other) tracking code here."),
       ),
	
	

	array(	"name" => __("Google Analytics"),
			"desc" => __("Please paste your Google Analytics tracking code"),
			"id" => $shortname."_google_analytics",
			"std" => "",
			"type" => "textarea"),
	
	array( "type" => "close"),	
	
	array( "name" => __("SEO Meta Tags"),  
       "type" => "section"),  	 
array( "type" => "open"), 



array( 	"name" => __("Author Name"),
			"desc" => __("Insert the author name of the blog"),
			"id" => $shortname."_seoname",
			"type" => "text",
			"std" => ""),
			
array( 	"name" => __("Blog Description"),
			"desc" => __("Insert a description of your blog, long as you wish."),
			"id" => $shortname."_seoname",
			"type" => "text",
			"std" => ""),
			
array( 	"name" => __("Author Contact Address"),
			"desc" => __("Insert an e-mail address as contact to your readers.(At your own risk)"),
			"id" => $shortname."_seocontact",
			"type" => "text",
			"std" => ""),
			
array( 	"name" => __("Copyright"),
			"desc" => __("The copyright of your blog"),
			"id" => $shortname."_seocopyright",
			"type" => "text",
			"std" => ""),
			
array( 	"name" => __("Keywords"),
			"desc" => __("Insert keywords for a good ranking on search engines.(Separated with commas)"),
			"id" => $shortname."_seokeywords",
			"type" => "text",
			"std" => ""),
			
array( 	"name" => __("Google Site Verification "),
			"desc" => __("from http://google.com/webmasters"),
			"id" => $shortname."_seogooglesite",
			"type" => "text",
			"std" => ""),
			

			
array( "type" => "close"), 

);
	





function mytheme_add_admin() {

    global $themename, $shortname, $options;

    if ( $_GET['page'] == basename(__FILE__) ) {
    
        if ( 'save' == $_REQUEST['action'] ) {

                foreach ($options as $value) {
                    update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }

                foreach ($options as $value) {
                    if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }

                header("Location: themes.php?page=dashboard.php&saved=true");
                die;

        } else if( 'reset' == $_REQUEST['action'] ) {

            foreach ($options as $value) {
                delete_option( $value['id'] ); 
                update_option( $value['id'], $value['std'] );}

            header("Location: themes.php?page=dashboard.php&reset=true");
            die;

        }
    }

      add_theme_page($themename." Options", "$themename Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');

}

function mytheme_add_init() {  
$file_dir=get_bloginfo('template_directory');  
wp_enqueue_style("functions", $file_dir."/dashboard/dashboard.css", false, "1.0", "all");  
wp_enqueue_script("hybridside_script", $file_dir."/dashboard/hybridside_script.js", false, "1.0");  
}  

function mytheme_admin() {

    global $themename, $shortname, $options;

    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';
    
    
?>
<div class="wrap hybridside_container">
<h2><?php echo $themename; ?> Theme Settings</h2>  
<form method="post">

<div class="hybridside_opts">
                        <?php foreach ($options as $value) {  
                        switch ( $value['type'] ) {  
                        case "open":  
                        ?>  
                          
                        <?php break;   
                        case "close":  
                        ?> 
</div>

                                        <input type="hidden" name="action" value="save" />
                                        </form>
                                        


</div>

<?php break; 
case "title":  
?> 
 
<p><?php _e('To manage the theme settings, you can use the menu below.'); ?> </p>  
<i><?php _e ('To change the background of body go to appereance > background '); ?> </i>  

<?php break;   
case 'text':  
?>  

                                <div class="hybridside_input hybridside_text">  
                                 <label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>  
                                 <input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id'])  ); } else { echo $value['std']; } ?>" />  
                                <small><?php echo $value['desc']; ?></small>
                                <div class="clearfix"></div>  
                                
                                </div>  
                     
<?php  
break;
case 'textarea':  
?>  

                                <div class="hybridside_input hybridside_textarea">  
                                <label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>  
                                <textarea name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id']) ); } else { echo $value['std']; } ?></textarea>  
                                <small><?php echo $value['desc']; ?></small><div class="clearfix"></div>  
                                
                                </div>  

<?php  
break;  
case 'select':  
?>  

                                <div class="hybridside_input hybridside_select">  
                                <label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>  
                                
                                <select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">  
                                <?php foreach ($value['options'] as $option) { ?>  
                                <option <?php if (get_settings( $value['id'] ) == $option) { echo 'selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?>  
                                </select>  
                                
                                <small><?php echo $value['desc']; ?></small><div class="clearfix"></div>  
                                </div>  

<?php  
break;  
case "checkbox":  
?>  

                                <div class="hybridside_input hybridside_checkbox">  
                                <label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>  
                                
                                <?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>  
                                <input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />  
                                
                                <small><?php echo $value['desc']; ?></small><div class="clearfix"></div>  
                                </div>  
<?php break; 
case "section":   
$i++;  
?> 
</form>
<div class="hybridside_section">
                                        <div class="hybridside_title">
                                        <h3><img src="<?php bloginfo('template_directory')?>/images/add.png" class="inactive" alt="""><?php echo $value['name']; ?></h3>
                                        <span class="submit"><input name="save<?php echo $i; ?>" type="submit" value="Save changes" /> </span>
                                        <div class="clearfix"></div>
                                        </div>
                                                                               
                                                                               
                                        <div class="hybridside_options">  
                                        
                                        <?php break;   
                                        }  
                                        }  
                                        ?> 

                                        <form method="post">
                                        <p class="submit">
                                        <input name="reset" type="submit" value="Reset" />
                                        <input type="hidden" name="action" value="reset" />
                                        </p>
                                        </form>
                                        
                                        
													 </div> 
                                        

</div>
<?php 
}  
add_action('admin_init', 'mytheme_add_init');  
add_action('admin_menu', 'mytheme_add_admin');  
?>