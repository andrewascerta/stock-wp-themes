<?php 
/*
Credits Pengbos.com --- Free wordpress themes & blogs
Creative Commons Attribution 2.5 License
Attribution:
     --- You must include the provided credit links to Pengbos.com in the page footer.
     --- Contact pengbos.com@gmail.com if you would like to remove the credit link.
*/
?>
<?php get_header(); ?>
<!-- main -->
<div id="main">
    <h2 class="inner"> <?php single_cat_title(); ?></h2>
    <div id="bits">
        <?php
        $count = 1;
        if (have_posts()) : while (have_posts()) : the_post();
                $project_name = get_post_meta($post->ID, "my_project_name", true);
                 $website_url = get_post_meta($post->ID, "my_web_url", true);
                $item_class = "";
                if ($count % 3 == 0)
                    $item_class = "last";
        ?>
                <div class="bit <?php echo $item_class; ?>">
                    <h4><a class="meta" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    <div class="photo">            
                        <a href="#p<?php echo $count ?>"><img src="<?php bloginfo('template_directory'); ?>/scripts/timthumb.php?src=<?php echo get_post_meta($post->ID, "my_portfolio_image", true) ?>&amp;h=70&amp;w=203&amp;zc=1" alt="<?php the_title(); ?>" /></a>
                    </div>            
                    <p><?php the_excerpt(); ?></p>
                    <p class="more"><a href="<?php echo $website_url; ?>">Visit Site</a></p>
                    <div id="p<?php echo $count ?>">
                        <img src="<?php echo get_post_meta($post->ID, "my_portfolio_image", true) ?>" alt="<?php the_title(); ?>" />
                    </div>            
                </div>          
<?php if ($count % 3 == 0): ?>
                    <div class="line"></div>            
<?php endif; ?>
        <?php $count++ ?>
        <?php endwhile;
                else: ?>
                    <h2>Woops...</h2>                    
                    <p>Sorry, no posts we're found.</p>                    
<?php endif; ?>
                    
                    <div class="clear"></div>            
                     <?php wp_link_pages(array('next_or_number' => 'next', 'previouspagelink' => ' &laquo; ', 'nextpagelink' => ' &raquo;')); ?>
                </div>            
                <!-- /portfolio bits -->		            
            </div>            
            <!-- /main -->            
            
<!-- side -->
<?php get_sidebar(); ?>
<!-- /side -->          
<?php get_footer(); ?>