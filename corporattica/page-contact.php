<?php 
/*
Credits Pengbos.com --- Free wordpress themes & blogs
Creative Commons Attribution 2.5 License
Attribution:
     --- You must include the provided credit links to Pengbos.com in the page footer.
     --- Contact pengbos.com@gmail.com if you would like to remove the credit link.
*/
?>
<?php
get_header();
?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/contact.js"></script>
			<!-- main -->
			<div id="main">
				<h2 class="inner">Contact Us</h2>
				<form id="contacts" method="post" action="">
					<p>
						<label for="name">Name:</label>
						<input type="text" class="text" name="name" id="name" />
                                                 <label id='name_error' class='error'>Insert a Name</label>    
					</p>
					<p>
						<label for="email">E-mail Address:</label>
						<input type="text" class="text" name="email" id="email" />
                                                <label id='email_error' class='error'>Enter a valid email address</label>    
					</p>
					<p>
						<label for="phone">Subject:</label>
						<input type="text" class="text" name="subject" id="subject" />
                                                 <label id='subject_error' class='error'>Enter a message subject</label>  
					</p>
					<p>
						<label for="message">Message:</label>
						<textarea class="text" name="message" id="message"></textarea>
                                                 <label id='message_error' class='error'>Enter a message</label> 
					</p>
                                                 <input type="hidden" name="your_email" value="<?php echo get_option('admin_email'); ?>">
                                                 <input type="hidden" name="your_web_site_name" value="<?php echo get_bloginfo('name'); ?>">
                                                 <p id='mail_success' class='success'>Thank you. We'll get back to you as soon as possible.</p>                
                                                 <p id='mail_fail' class='error'>Sorry, an error has occured. Please try again later.</p>       
                                
					<p>
						<input type="submit" id='send_message' class="submit" value="Send" />
					</p>
				</form>
			</div>
			<!-- /main -->
			
			<!-- side -->
			<?php get_sidebar('contact'); ?>
			<!-- /side -->
<?php get_footer(); ?>