<?php 
/*
Credits Pengbos.com --- Free wordpress themes & blogs
Creative Commons Attribution 2.5 License
Attribution:
     --- You must include the provided credit links to Pengbos.com in the page footer.
     --- Contact pengbos.com@gmail.com if you would like to remove the credit link.
*/
?>
<?php get_header(); ?>
<!-- main -->
<div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <h2><?php the_title(); ?></h2>
        
    <?php the_content(); ?>
        
    <?php endwhile;
        else: ?>
            <p>Sorry, no page found</p>                
    <?php endif; ?>
        </div>        
        <!-- /main -->        
        
        <!-- side -->        
        <?php get_sidebar(); ?>   
        <!-- /side -->        
<?php get_footer(); ?>