LICENSE

Creative Commons Attribution 2.5 License
You are free:

    * to Share - to copy, distribute, display, and perform the work
    * to Remix - to make derivative works

Under the following conditions:

    * Attribution. You must attribute the work in the manner specified by the author or licensor.
    * For any reuse or distribution, you must make clear to others the license terms of this work.
    * Any of these conditions can be waived if you get permission from the copyright holder.

Attribution:

    * You must include the provided credit links to Pengbos.com in the page footer.
      Contact us if you would like to remove the credit link.


GUIDE:
The installation guide is posted on the website: http://pengbos.com. You can find the step by step installation guide there.



