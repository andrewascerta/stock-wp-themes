<?php 
/*
Credits Pengbos.com --- Free wordpress themes & blogs
Creative Commons Attribution 2.5 License
Attribution:
     --- You must include the provided credit links to Pengbos.com in the page footer.
     --- Contact pengbos.com@gmail.com if you would like to remove the credit link.
*/
?>
<?php 
get_header(); ?>		
			<!-- main -->
			<div id="main">
				<h2 class="inner"> <?php single_cat_title(); ?></h2>
                         <div id="page">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            				<!-- blog post -->
            <div class="blog-post">
                    <p class="blog-date"><span><?php the_time('jS'); ?></span><br /><?php the_time('F'); ?></p>
                    <div class="blog-body">
                            <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                           <p class="sub"><?php the_category(', '); ?> </p>
                            <?php the_content('Read more...'); ?> <a href="<?php the_permalink() ?>">&raquo;</a>
                    </div>
                    <div class="clear"></div>
            </div>
            <?php endwhile;
                else: ?>

                    <h2>Woops...</h2>        

                    <p>Sorry, no posts we're found.</p>        

        <?php endif; ?>
                     <?php wp_link_pages(array('next_or_number' => 'next', 'previouspagelink' => ' &laquo; ', 'nextpagelink' => ' &raquo;')); ?>
			</div>
		
			</div>
			<!-- /main -->
			
<!-- side -->
<?php get_sidebar(); ?>
<!-- /side -->
<?php get_footer(); ?>	