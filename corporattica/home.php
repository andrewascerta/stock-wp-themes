<?php 
/*
Credits Pengbos.com --- Free wordpress themes & blogs
Creative Commons Attribution 2.5 License
Attribution:
     --- You must include the provided credit links to Pengbos.com in the page footer.
     --- Contact pengbos.com@gmail.com if you would like to remove the credit link.
*/
?>
<?php get_header(); ?>		
<!-- main -->
<div id="main">
    <div id="intro">
        <?php
        $introquery = new WP_Query( array( 'post_type' => 'homepage', 'posts_per_page' => 1, 'section' => 'introduction' ) );
        while ($introquery->have_posts()) : $introquery->the_post();
        ?>
        <h2><?php the_title(); ?></h2>
        <p><?php the_content(); ?></p>
       <?php endwhile; ?>
    </div>

    <!-- bits -->
    <div id="bits">
        <?php
        $count = 1;
        $args = array('post_type' => 'homepage', 'posts_per_page' => 3, 'section' => 'bits');
        $loop = new WP_Query($args);
        while ($loop->have_posts()) : $loop->the_post();
            $item_class = "";
            if ($count == 3)
                $item_class = "last";
        ?>
            <div class="bit <?php echo $item_class; ?>">
                <h4><?php the_title(); ?></h4>
                <div class="photo">    
                    <a href="#portfolio<?php echo $count ?>"><img src="<?php bloginfo('template_directory'); ?>/scripts/timthumb.php?src=<?php echo get_post_meta($post->ID, "my_post_image", true) ?>&amp;h=70&amp;w=203&amp;zc=1" alt="<?php the_title(); ?>" /></a>
                </div>    
                <?php the_excerpt(); ?>
                <p class="more"><a href="<?php the_permalink(); ?>">Read More</a></p>
                <div id="portfolio<?php echo $count ?>">
                    <img src="<?php echo get_post_meta($post->ID, "my_post_image", true) ?>" alt="<?php the_title(); ?>" />
                </div>    
            </div>    
        <?php
            $count++;
        endwhile; ?>
        <div class="clear"></div>
    </div>
    <!-- /bits -->

</div>
<!-- /main -->

<!-- side -->
<?php get_sidebar('home'); ?>
<!-- /side -->
<?php get_footer(); ?>		

