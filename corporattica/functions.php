<?php

/*
Credits Pengbos.com --- Free wordpress themes & blogs
Creative Commons Attribution 2.5 License
Attribution:
     --- You must include the provided credit links to Pengbos.com in the page footer.
     --- Contact pengbos.com@gmail.com if you would like to remove the credit link.
*/

add_action( 'init', 'register_my_menu' );

function register_my_menu() {
	register_nav_menu( 'primary', __( 'Primary Menu' ) );
        register_nav_menu( 'footer', __( 'Footer Menu' ) );
}

add_action('init', 'homepage_register');
 // portfolio custom type is not activated.
function portfolio_register() {
	$labels = array(
		'name' => _x('Portfolio', 'post type general name'),
		'singular_name' => _x('Portfolio Item', 'post type singular name'),
		'add_new' => _x('Add New', 'portfolio item'),
		'add_new_item' => __('Add New Portfolio Item'),
		'edit_item' => __('Edit Portfolio Item'),
		'new_item' => __('New Portfolio Item'),
		'view_item' => __('View Portfolio Item'),
		'search_items' => __('Search Portfolio'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/images/portfolio.gif',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail')
	  ); 
 
	register_post_type( 'portfolio' , $args );
}

function homepage_register() {
	$labels = array(
		'name' => _x('Home Page', 'post type general name'),
		'singular_name' => _x('Home Item', 'post type singular name'),
		'add_new' => _x('Add New', 'portfolio item'),
		'add_new_item' => __('Add New Home Page Item'),
		'edit_item' => __('Edit Home Page Item'),
		'new_item' => __('New Home Page Item'),
		'view_item' => __('View Home Page Item'),
		'search_items' => __('Search Home Page Item'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/images/homepage.gif',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail')
	  ); 
 
	register_post_type( 'homepage' , $args );
        flush_rewrite_rules();
}
register_taxonomy("section", array("homepage"), array("hierarchical" => true, "label" => "Sections", "singular_label" => "Home Page Sections", "rewrite" => true));

if(!term_exists('introduction','section')){
    wp_insert_term(
      'introduction', // the term 
      'section', // the taxonomy
      array(
        'description'=> 'Introduction in the Home Page',
        'slug' => 'introduction',
        'parent'=> 0
      )
    );
}
if(!term_exists('bits','section')){
    wp_insert_term(
      'bits', // the term 
      'section', // the taxonomy
      array(
        'description'=> 'Bits in the Home Page',
        'slug' => 'bits',
        'parent'=> 0
      )
    );
}

if(!term_exists('featured','category')){
    wp_insert_term(
      'featured', // the term 
      'category', // the taxonomy
      array(
        'description'=> 'Featured Post',
        'slug' => 'featured',
        'parent'=> 0
      )
    );
}

if(!term_exists('blog','category')){
    wp_insert_term(
      'blog', // the term 
      'category', // the taxonomy
      array(
        'description'=> 'Blog Post',
        'slug' => 'blog',
        'parent'=> 0
      )
    );
}

if(!term_exists('portfolio','category')){
    wp_insert_term(
      'portfolio', // the term 
      'category', // the taxonomy
      array(
        'description'=> 'Portfolio Post',
        'slug' => 'portfolio',
        'parent'=> 0
      )
    );
}

add_action('admin_print_scripts', 'admin_scripts');

function admin_scripts() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_register_script('mytheme_upload', get_bloginfo('template_url') . '/js/script.js', array());
    wp_enqueue_script('mytheme_upload');
}

# Enqueue JS
function mytheme_js() {
  if (is_admin()) return;
    wp_enqueue_script('jquery');
	wp_register_script('mytheme_menu', get_bloginfo('template_url') . '/js/menu.js', array());
    wp_enqueue_script('mytheme_menu');
	wp_register_script('mytheme_cufon', get_bloginfo('template_url') . '/js/cufon-yui.js', array());
    wp_enqueue_script('mytheme_cufon');
	
	wp_register_script('mytheme_Arial', get_bloginfo('template_url') . '/js/Arial.font.js', array());
    wp_enqueue_script('mytheme_Arial');
	
	wp_register_script('mytheme_jquery_easing', get_bloginfo('template_url') . '/js/jquery.easing.js', array());
    wp_enqueue_script('mytheme_jquery_easing');
	
	wp_register_script('mytheme_lofscript', get_bloginfo('template_url') . '/js/lofscript.js', array());
    wp_enqueue_script('mytheme_lofscript');
	
		wp_register_script('mytheme_fancyzoom', get_bloginfo('template_url') . '/js/fancyzoom.min.js', array());
    wp_enqueue_script('mytheme_fancyzoom');
	
}
add_action('init', mytheme_js);

include(TEMPLATEPATH . '/post_custom_form.php');
include(TEMPLATEPATH . '/custom_form.php');


if (function_exists('register_sidebar'))
	    register_sidebar( array(
	    'id'          => 'right',
	    'name'        => __( 'Right Sidebar', $text_domain ),
	    'description' => __( 'This is the right sidebar for almost every page', $text_domain ),
	) );
	
if (function_exists('register_sidebar'))
	    register_sidebar( array(
	    'id'          => 'home',
	    'name'        => __( 'Homepage Sidebar', $text_domain ),
	    'description' => __( 'This is the sidebar in the home page', $text_domain ),
	) );	
	
	
if (function_exists('register_sidebar'))
	    register_sidebar( array(
	    'id'          => 'contact',
	    'name'        => __( 'Contact Sidebar', $text_domain ),
	    'description' => __( 'This is the sidebar in the contact page', $text_domain ),
	) );		

function new_excerpt_length($length) {
     global $post;
     if(get_post_type( $post ) == 'homepage')
	return 15;
    else {
        return 20;
    }
}
add_filter('excerpt_length', 'new_excerpt_length');

function new_excerpt_more($more) {
       global $post;
	return '<a href="'. get_permalink($post->ID) . '">...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

?>