<?php 
/*
Credits Pengbos.com --- Free wordpress themes & blogs
Creative Commons Attribution 2.5 License
Attribution:
     --- You must include the provided credit links to Pengbos.com in the page footer.
     --- Contact pengbos.com@gmail.com if you would like to remove the credit link.
*/
?>
		</div>
		<!-- footer -->
		<div id="footer">
			<div id="footerbg">
				<div class="wrap">
                     <!-- footer links -->
				       <?php wp_nav_menu( array( 'container' => '','menu_id'=>'footer_menu' ,'theme_location' => 'footer') ); ?>
					<!-- /footer links -->
					
					<!-- credit link, please don't remove the credit link, if you want to put your name on it, contact pengbos.com@gmail.com -->
					<p id="copy">Copyright<a href="http://pengbos.com" title="Free wordpress theme">Pengbos.com</a> CSS from <a href="http://www.solucija.com" title="Free CSS Templates">Solucija</a>- remove upon purchase
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<!-- /footer -->
		<?php 
			wp_footer();
		?>
	</div>
</body>
</html>