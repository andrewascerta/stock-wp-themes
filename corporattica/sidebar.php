<?php 
/*
Credits Pengbos.com --- Free wordpress themes & blogs
Creative Commons Attribution 2.5 License
Attribution:
     --- You must include the provided credit links to Pengbos.com in the page footer.
     --- Contact pengbos.com@gmail.com if you would like to remove the credit link.
*/
?>
<div id="side">
    <ul class="side-menu">
        <?php if ( function_exists ( dynamic_sidebar('right') ) ) : ?>

            <?php dynamic_sidebar ('right'); ?>

        <?php endif; ?>
    </ul>
</div>   