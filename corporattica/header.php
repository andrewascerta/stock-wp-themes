<?php 
/*
Credits Pengbos.com --- Free wordpress themes & blogs
Creative Commons Attribution 2.5 License
Attribution:
     --- You must include the provided credit links to Pengbos.com in the page footer.
     --- Contact pengbos.com@gmail.com if you would like to remove the credit link.
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<title>
             <?php
            if (is_home()) {
                echo bloginfo('name');
            } elseif (is_404()) {
                echo '404 Not Found';
            } elseif (is_category()) {
                echo bloginfo('name');
                wp_title('');
            } elseif (is_search()) {
                echo 'Search Results';
            } elseif (is_day() || is_month() || is_year()) {
                echo 'Archives:';
                wp_title('');
            } else {
                echo wp_title('');
            }
            ?>
        </title>
        <meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
        <meta name="description" content="<?php bloginfo('description') ?>" />
        <meta name="theme_template_dir" content="<?php bloginfo('template_directory'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
		<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>        
	<script type="text/javascript" charset="utf-8">
jQuery.noConflict();
                var theme_template_dir = jQuery("meta[name=theme_template_dir]").attr('content');
		jQuery(document).ready(function($) {
			$('div.photo a').fancyZoom({directory: theme_template_dir + '/images/zoom', scaleImg: true, closeOnClick: true});

                var buttons = { previous:$('#lofslidecontent45 .lof-previous') ,
				next:$('#lofslidecontent45 .lof-next') };
							
		$obj = $('#lofslidecontent45').lofJSidernews( { interval                : 5000,
								easing			: 'easeInOutQuad',	
								duration		: 1200,
								auto		 	: true,
								mainWidth               :960,
								buttons			: buttons} );	
		});
                Cufon.replace('h1,h2,h3,h4,h5,#menu,#copy,.blog-date');
	</script>
</head>
    <body>
        <div id="bg">
            <div class="wrap">
                <!-- logo -->
                <h1><a href="<?php echo get_settings('home'); ?>"><?php echo bloginfo('name'); ?></a></h1>
                <!-- /logo -->
                <!-- menu -->
                <div id="mainmenu">
                    <?php wp_nav_menu(array('container' => '', 'menu_id' => 'menu', 'theme_location' => 'primary')); ?>
                </div>
                <!-- /menu -->   
                <!-- pitch -->
                <div id="pitch">
                    <!------------------------------------- THE CONTENT ------------------------------------------------->
                    <div id="lofslidecontent45" class="lof-slidecontent" style="width:960px; height:340px;">
                        <div class="preload"><div></div></div>
                        <!-- MAIN CONTENT --> 
                        <div class="lof-main-outer" style="width:960px; height:340px;">
                            <div onclick="return false" href="" class="lof-previous">Previous</div>
                            <ul class="lof-main-wapper">
                                <?php
                                $the_query = new WP_Query('category_name=featured');
                                while ($the_query->have_posts()) : $the_query->the_post(); ?>
                                    <li>    
                                        <img src="<?php bloginfo('template_directory'); ?>/scripts/timthumb.php?src=<?php echo get_post_meta($post->ID, "my_post_image", true) ?>&amp;h=340&amp;w=960&amp;zc=1" title="<?php the_title_attribute(); ?>" />           
                                        <div class="lof-main-item-desc">    
                                            <h3><a target="_parent" title="<?php the_title_attribute(); ?>" href="<?php the_permalink() ?>">/ <?php the_title(); ?> /</a> <a href="#"><i> <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> </i></a></h3>
                                            <p><?php the_excerpt(); ?>
                                                <a class="readmore" href="<?php the_permalink() ?>">Read more </a>
                                            </p>    
                                        </div>    
                                    </li>     
                                <?php endwhile; ?>
                            </ul>  	
                            <div onclick="return false" href="" class="lof-next">Next</div>
                        </div>
                        <!-- END MAIN CONTENT --> 
                        <!-- NAVIGATOR -->
                        <!----------------- --------------------->
                    </div> 
                </div>
                <!-- /pitch -->
                    