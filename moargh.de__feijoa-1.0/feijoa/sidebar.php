<div id="sidebar">


	<!-- TITLE -->
		
	<a href="#">
		<img src="<?php bloginfo('template_url'); ?>/img/this-is-a.png" alt="" />
	</a>
	
	<! -- END TITLE -->
	
	
	<!-- NAVIGATION -->
	
	<ul id="navigation">
		<li><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?></li>
	</ul> 
		
	<!-- END NAVIGATION -->	
	

	<!-- CREDITS -->

	<div id="credits">
		<div class="sub">
			Wordpress theme by<br />
			<a href="http://moargh.de" target="_blank">Moargh.de</a>
		</div>			
	</div>		
	
	<!-- END CREDITS -->
	
	
</div> <!-- end sidebar -->