<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">


<head>
	
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
   	<meta name="AUTHOR" content="" />
	<meta name="DESCRIPTION" content="" />
	<meta name="ROBOTS" content="index, follow" />
	<meta name="LANGUAGE" content="de" />
    
    <title>Wordpress Theme by Moargh.de: Feijoa</title>
    
  	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/style.css" media="screen" charset="utf-8" />
	
	<?php if ( is_singular() ) { ?>
	<link href="<?php bloginfo('template_url'); ?>/css/single.css" rel="stylesheet" type="text/css" />
	<? } else {} ?>
	
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.4.1.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.masonry.js"></script>
       	
	<?php wp_head() ?>

</head>


<body>

			
	<div id="container">