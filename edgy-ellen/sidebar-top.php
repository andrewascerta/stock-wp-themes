<div id="top-widget-area-wrapper" role="wrapper">
<div id="top-widget-area" class="widget-area_top" role="complementary">

	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Top 1') ) : ?>
		<div id="first" class="widget-area_top">
			<div id="first-widget"></div>
			<h3 class="widget-title-top first" >Widget 1</h3>
			<div class="interior">						
				The US leads the world in numbers of Windows PCs that are part of botnets, reveals a report.More than 2.2 million US PCs were found to be part of botnets, networks of hijacked home computers, in the first six months of 2010, it said						
			</div>
		</div>
	<?php endif; ?>
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Top 2') ) : ?>
		<div id="second" class="widget-area_top">	
			<div id="second-widget"></div>		
			<h3 class="widget-title-top second">Widget 2</h3>
			<div class="interior">						
				The US leads the world in numbers of Windows PCs that are part of botnets, reveals a report.
More than 2.2 million US PCs were found to be part of botnets, networks of hijacked home computers, in the first six months of 2010, it said				
			</div>
		</div>
	<?php endif; ?>
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Top 3') ) : ?>
		<div id="third" class="widget-area_top">
			<div id="third-widget"></div>		
			<h3 class="widget-title-top third">Recent Comments</h3>
			<div class="interior">						
				The US leads the world in numbers of Windows PCs that are part of botnets, reveals a report.			
More than 2.2 million US PCs were found to be part of botnets, networks of hijacked home computers, in the first six months of 2010, it said				
			</div>
		</div>
	<?php endif; ?>

</div><!-- #top .widget-area -->
</div><!-- #top .widget-area-wrapper -->