<?php

define( GOO_DIR, TEMPLATEPATH );
define( GOO_LIB, GOO_DIR . '/includes' );
define( THEME_ID, apply_filters('goo_theme_id', 'edgy-ellen') );
define( THEME_VERSION, $theme_data['Version'] );
define(GOO_FEAT_THUMB_W,570);
define(GOO_FEAT_THUMB_H,251);
define(GOO_LIST_THUMB_W,170);
define(GOO_LIST_THUMB_H,170);
define(GOO_OLDPOST_THUMB_W,50);
define(GOO_OLDPOST_THUMB_H,50);


require_once GOO_LIB . '/admin-functions.php';
require_once GOO_LIB . '/helpers.php';
require_once GOO_LIB . '/options.php';
require_once GOO_LIB . '/walker.php';

goo_flush_options();

require_once GOO_LIB . '/core.php';
require_once GOO_LIB . '/twitter.php';

if ( ! isset( $content_width ) )
	$content_width = 897;

add_action( 'after_setup_theme', 'goo_setup' );
add_shortcode('ad', 'goo_advertisement');

set_post_thumbnail_size(GOO_LIST_THUMB_W,GOO_LIST_THUMB_H,true);
add_image_size('featured',GOO_FEAT_THUMB_W,GOO_FEAT_THUMB_H,true);
add_image_size('olderpost',GOO_OLDPOST_THUMB_W,GOO_OLDPOST_THUMB_H,true);

if ( ! function_exists( 'goo_setup' ) ):

function goo_setup() {

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'goo', TEMPLATEPATH . '/languages' );

	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'goo' ),
	) );

	// This theme allows users to set a custom background
	//add_custom_background();

}
endif;

function goo_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'goo_page_menu_args' );

function goo_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'goo_excerpt_length' );

function goo_continue_reading_link() {
	//return '<span class="meta-nav-home"> <a href="'. get_permalink() . '">' . __( 'read more', 'goo' ) . '</a></span>';
}

function goo_auto_excerpt_more( $more ) {
	return ' &hellip;' . goo_continue_reading_link();
}
add_filter( 'excerpt_more', 'goo_auto_excerpt_more' );

function goo_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= goo_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'goo_custom_excerpt_more' );

function goo_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
add_filter( 'gallery_style', 'goo_remove_gallery_css' );

if ( ! function_exists( 'goo_comment' ) ) :

function goo_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 62 ); ?>
			<?php printf( __( '%s <span class="says">says:</span>', 'goo' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em><?php _e( 'Your comment is awaiting moderation.', 'goo' ); ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'goo' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'goo' ), ' ' );
			?>
		</div><!-- .comment-meta .commentmetadata -->

		<div class="comment-body"><?php comment_text(); ?></div>

		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div><!-- .reply -->
	</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'goo' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'goo'), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;

/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override goo_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Twenty Ten 1.0
 * @uses register_sidebar
 */
function goo_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'goo' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'goo' ),
		'before_widget' => '<li id="%1$s" class="widget-container-primary %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title-primary">',
		'after_title' => '</h3>',
	) );




}
/** Register sidebars by running goo_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'goo_widgets_init' );

/**
 * Removes the default styles that are packaged with the Recent Comments widget.
 *
 * To override this in a child theme, remove the filter and optionally add your own
 * function tied to the widgets_init action hook.
 *
 * @since Twenty Ten 1.0
 */
function goo_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'goo_remove_recent_comments_style' );

if ( ! function_exists( 'goo_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post—date/time and author.
 *
 * @since Twenty Ten 1.0
 */
function goo_posted_on() {
	printf( __( '<span class="%1$s"> %2$s</span>  ', 'goo' ),
		'meta-date',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		)
						
	);
}
endif;

if ( ! function_exists( 'goo_posted_in' ) ) :
/**
 * Prints HTML with meta information for the current post (category, tags and permalink).
 *
 * @since Twenty Ten 1.0
 */
function goo_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'goo' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'goo' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'goo' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;


