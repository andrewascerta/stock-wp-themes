<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="container">
			<div id="content" role="main">				
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<div class="entry-title-wrap"><h1 class="entry-title" ><?php the_title(); ?></h1></div>

					<div class="entry-meta">
						<?php the_time('F j, Y '); ?> | <span class="dashed"><?php the_category('  ,  ');?></span>
					</div><!-- .entry-meta -->
				
					<div class="entry-content">
						
							<div class="social-media">
								<div id="twit">
									<a href="http://twitter.com/share" class="twitter-share-button" data-count="vertical">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
								</div>
								<div id="fb">
								<a name="fb_share" type="box_count" href="http://www.facebook.com/sharer.phpu=<?php the_permalink() ?>&t=<?php the_title(); ?>" share_url="<?php the_permalink() ?>">Share</a>
								<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
								</div>

							</div>	
					
						<?php the_content(); ?>
						
					</div><!-- .entry-content -->	
	<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
				
					<div id="entry-author-info">
					<h3><?php printf( esc_attr__( 'About %s', 'twentyten' ), get_the_author() ); ?></h2>
						<div id="author-avatar">
							<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyten_author_bio_avatar_size', 60 ) ); ?>
						</div><!-- #author-avatar -->
						<div id="author-description">							
							<?php the_author_meta( 'description' ); ?>
							<div id="author-link">
								<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
									<?php printf( __( 'View all posts by %s <span class="meta-nav"></span>', 'twentyten' ), get_the_author() ); ?>
								</a>
							</div><!-- #author-link	-->
						</div><!-- #author-description -->
					</div><!-- #entry-author-info -->
<?php endif; ?>				
					
				<div id="sharer">
					<h3 id="sharer-title">Share This Post</h3>
					<?php social_bookmark_share(urlencode(get_permalink()),urlencode(get_the_title()),urlencode(get_the_excerpt()));?>

				</div>

				<?php comments_template( '', true ); ?>
			
			</div><!-- #post -->
			</div><!-- #content -->
<?php get_sidebar(); ?>

</div><!-- #container -->
<?php get_footer(); ?>
