<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>


		<div id="container">
			<div id="content" role="main" >
			
				<?php if (is_home() && goo_get_option('disable_gallery')==0) : ?>
					<div id="featured">
					<div class="pad">	
						<div class="box box-featured">
							<div class="interior" style="background:#000;">							
								<?php goo_featured_posts(); ?>
							</div>
						</div>
					</div>
				</div>

				<?php endif; ?>			
					<?php if(goo_get_option('banner_468_60_1')<>'' ): ?>
					<div class="banner-wrapper">
					<?php echo stripslashes(goo_get_option('banner_468_60_1'));?>
					</div>
					<?php endif;?>			
				<div class="post-group">
				<?php
					get_template_part( 'loop', 'index' );
				?>
				</div>
				
				
				<!-- #start of older posts -->
				
				
				<!-- #end of older posts -->
				
			</div><!-- #content -->
				<?php get_sidebar(); ?>
		</div><!-- #container -->
		


<?php get_footer(); ?>
