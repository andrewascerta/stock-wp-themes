<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
			<ul class="xoxo">
			
			<li id="about" class="widget-container-primary">
				<h3 class="widget-title-primary"><?php _e( 'About me', 'goo' ); ?></h3>
				<div class="interior">						
						<?php if ( goo_get_option('about_info') <> '' && goo_get_option('about_info_img') <> '') {
								echo '<p>' . '<img src="' . goo_get_option('about_info_img') . '"/>' . stripslashes(goo_get_option('about_info')) . '</p>' ;
							}		
							else{
								echo '<p>' . stripslashes(goo_get_option('about_info')) . '</p>' ;
							}
						?>
				</div>
			</li>
			
<?php
		if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>

			<li id="sponsors" class="widget-container-primary">
				<h3 class="widget-title-primary"><?php _e( 'Sponsors', 'goo' ); ?></h3>
				<ul class="ads">
					<?php if(goo_get_option('banner_250')<>'' ): ?>
					<?php echo goo_get_option('banner_250');?>
					<?php endif;?>
				</ul>
			</li>
			<li id="search" class="widget-container-primary">
				<div id="site-search" >
						<?php get_search_form(); ?>
				</div><!-- #search form -->
			</li>
				<li id="socials">
					<h3 class="widget-title-primary">Subscribe & Follow </h3> 
					<?php goo_social_nav(); ?>
				</li><!-- #socials -->			
			<li id="popular-posts" class="widget-container-primary">
				<h3 class="widget-title-primary"><?php _e( 'Popular Posts', 'goo' ); ?></h3>
				<ul class="popular-post-items">
					<?php 
					get_mostpopular('pages=0&stats_comments=1&range=all&limit=5&thumbnail_width=90&thumbnail_height=90&do_pattern=1&pattern_form={image}{title}{stats}');  ?>
				</ul>
			</li>
			
			<li id="tweets" class="widget-container-primary">

				<h3 class="widget-title-primary"><?php _e( 'Follow me in Twitter', 'goo' ); ?></h3>

						<?php $twitter_id = goo_get_option('twitter_id');							

						echo Goo_Twitter::get_messages($twitter_id,3,true);

						?>		

			</li>

			<li id="blogroll" class="widget-container-primary">

				<h3 class="widget-title-primary"><?php _e( 'Blogroll', 'goo' ); ?></h3>

				<ul class="sidebar-widgets">

					<?php wp_list_bookmarks('title_li=&categorize=0'); ?>

				</ul>

			</li>			
			<li id="flickr-photos" class="widget-container-primary">
				<h3 class="widget-title-primary"><?php _e( 'Featured Photos', 'goo' ); ?></h3>
				<ul class="flickr">
					<?php get_flickrRSS(array('html' => '<a href="%flickr_page%" title="%title%"><div class="flickr_cont"><img src="%image_square%" alt="%title%"/></div></a>')); ?>					
				</ul>
			</li>
			 <?php if(goo_get_option('video_id')<>'' ): ?>
			<li id="video-embed" class="widget-container-primary">
				<h3 class="widget-title-primary"><?php _e( 'Featured Video', 'goo' ); ?></h3>
				 <?php echo embed_video(); ?>
			</li>	
			<?php endif;?>
		<?php endif; // end primary widget area ?>
			</ul>
	
		
		</div><!-- #primary .widget-area -->
