<?php
/**
 * The Search Form
 *
 * Optional file that allows displaying a custom search form
 * when the get_search_form() template tag is used.
 *
 */
?>
<form method="get" id="searchform" action="<?php echo home_url(); ?>" >
	<div>		
		<span class="input"><input type="text" value="<?php _e('Search this site', 'goo') ?>" name="s" id="s" onfocus="this.value=''" class="text" /></span>
		
	</div>
</form>