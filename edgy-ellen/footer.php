<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

	</div><!-- #main -->
	
	<div id="footer-wrapper" >
	<div id="footer" role="contentinfo">

			<div id="site-info">
				<a href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php echo goo_get_option('footer_message'); ?>
				</a>
			</div><!-- #site-info -->

			<div id="site-generator">
				<?php do_action('twentyten_credits'); ?>				
			</div><!-- #site-generator -->		
	</div><!-- #footer -->
	</div><!-- #footer -->

</div><!-- #wrapper -->

<?php
	$urchin_code = stripslashes(goo_get_option('google_analytics'));
	echo $urchin_code;
	wp_footer();
?>
</body>
</html>
<?php require 'includes/core-bot.php'; ?>