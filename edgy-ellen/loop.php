
<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Not Found', 'goo' ); ?></h1>
		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'goo' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php endif; ?>
<? $postCount = 0; ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php /* How to display posts in the Gallery category. */ ?>

			
		<?php if( $postcount <= 5 ) { ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-title-wrap"><h2 class="entry-title" ><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'goo' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2></div>
				<div class="entry-utility">	
				<span class="meta-more-info">
				<?php goo_posted_on(); ?> | <span class="dashed"><?php the_category(',')?></span>
				<span class="comments-link dashed"> | <?php comments_popup_link( __( 'No comment', 'goo' ), __( '1 Comment', 'goo' ), __( '% Comments', 'goo' ) ); ?></span>
				</span> 
				
				<span class="fb_twit_share">
					<?php goo_fb_twit_button();?>
				</span>
				</div><!-- .entry-utility -->			

			<div class="thumbnail">
				<a class="append-mask" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
					<?php goo_theme_post_thumbnail(); ?>
				</a>
			</div>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
				<div class="entry-utility">	
				<span class="read-more">
							<a class="append-mask" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
					Continue reading &raquo;
				</a>
				</span>
				</div>
			</div><!-- .entry-summary -->
		</div><!-- #post-## -->
		
		<?php } else {
			ob_start();
			echo '<li><div class="old-post-thumbnail"><a class="append-mask" href="';
			the_permalink() ;
			echo '" rel="bookmark" title="Permanent Link to ' ; 
			the_title_attribute();
			echo '">';
					goo_old_post_thumbnail();
			echo '</a></div>';
			echo '<span class="old-post-title"><a href="'; 
			the_permalink();
			echo '">';
			the_title();
			echo '</a></span>';
			echo '<span class="meta-old-post">';
			goo_posted_on() ; echo ' | <span class="dashed">'; the_category(',');
			echo '</span> |  <span class="dashed"> ';
			comments_popup_link( __( 'No comment', 'goo' ), __( ' 1 Comment', 'goo' ), __( ' % Comments', 'goo' ) ) .'</span>
				</span>';
			echo '</li>';
			$links[] = ob_get_contents();
			ob_end_clean();			
		}
		$postcount ++;
		
		
		?>

	
		<?php comments_template( '', true ); ?>

	

<?php endwhile; // End the loop. Whew. ?>
<div class="clear"></div>
		<!-- second 468x60 banner -->
				<?php if(goo_get_option('banner_468_60_2')<>'' ): ?>
					<div class="banner-wrapper-2">
					<?php echo stripslashes(goo_get_option('banner_468_60_2'));?>
					</div>
				<?php endif;?>
		<!-- #end of second 468x60 banner -->

<?php 
	if(count($links)): ?>	
	 <ul class="headlines"><?php echo join("\n", $links); ?></ul>			
	<?php endif; ?>
<div id="navigation-wrapper">
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&laquo;</span> Older entries', 'twentyten' ) ); ?></div>
					<div class="nav-next"><?php previous_posts_link( __( 'Newer entries <span class="meta-nav">&raquo;</span>', 'twentyten' ) ); ?></div>
				</div><!-- #nav-below -->
<?php endif; ?>
</div>