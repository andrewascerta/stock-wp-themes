<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="container">
			<div id="content" role="main">
			
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>			
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<div class="entry-title-wrap"><h1 class="entry-title" ><?php the_title(); ?></h1></div>
			
					<div class="entry-content">

						<?php the_content(); ?>
						
					</div><!-- .entry-content -->	
<?php endwhile; ?>			
			</div><!-- #post -->
			</div><!-- #content -->
<?php get_sidebar(); ?>

</div><!-- #container -->
<?php get_footer(); ?>
