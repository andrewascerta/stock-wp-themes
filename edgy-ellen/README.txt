Thank you for downloading Edgy Ellen WordPress Theme. 

Download the latest version at http://wpclassic.com/edgy-ellen.

Requirement
===========
1. Web server with PHP 5 and MySQL Database
2. PHP5 support GD Library
3. Wordpress 3.0+

Install the Required Plugins
=========================
1. FlickrRSS - http://wordpress.org/extend/plugins/flickr-rss/
2. Regenerate Thumbnails - http://wordpress.org/extend/plugins/regenerate-thumbnails/
3. Wordpress Popular Posts - http://wordpress.org/extend/plugins/wordpress-popular-posts
4. WP-PageNavi - http://wordpress.org/extend/plugins/wp-pagenavi/

Changing the Logo
================
1. Find the logo.psd at edgy-ellen folder > images > logo.psd
2. Edit the file in Adobe Photoshop.
3. Save it as logo.png.
4. Upload in your server at themes > edgy-ellen > images > folder

Customizing the Theme
==============
1. Login in the Admin Panel.
2. Go to Appearance > Edgy Ellen Options
3. Change the different settings at the Main, Featured Content, Miscellaneous and Advertisement sections.
4. Save Changes.

License Agreement
=================
The theme is released under GNU General Public License. 
You may use it for all your projects for free and without any restrictions but we appreciate it if you will link back to us so that others may know where to find our free WordPress themes.

Warranty
=========
Our themes are provided "as is" without warranty of any kind, either expressed or implied. We do not guarantee that themes will work in all browsers, nor do we guarantee that our themes will be functional with all versions of WordPress. We do not guarantee compatibility with any additional third party plugins, scripts, or applications. 

Warranty
=========
We are not responsible for any damage while using this theme, use it at your own risk.

Support and Customization
======================
If you need support and customization, feel free to contact us at http://wpclassic.com/contact/.
