<?php 

function goo_admin_tabs() {
	$_default_tabs = array(
		'main'		=>	__('Main Settings', 'goo'),
		'categories'	=>	__('Featured Content Settings', 'goo'),		
		'layout'		=>	__('Miscellaneous Settings', 'goo'),
		'design'		=>	__('Advertisement', 'goo'),
	);	
	
	return apply_filters('goo_admin_tabs', $_default_tabs);
}

add_action('goo_admin_main', 'goo_admin_main_form');
add_action('goo_admin_categories', 'goo_admin_categories_form');
add_action('goo_admin_layout', 'goo_admin_layout_form');
add_action('goo_admin_design', 'goo_admin_design_form');
add_action('goo_admin_reset', 'goo_admin_reset_form');

?>
<div class="wrap">
<h2> Edgy Ellen settings</h2>

<form method="post" enctype="multipart/form-data" id="goo-settings-form" action="themes.php?page=edgy-options&_wpnonce=<?php echo $nonce ?>">

		<ul id="goo-tabs" class="clearfix">
			<?php $tabs = array_merge( goo_admin_tabs(), array('reset' => __('Reset', 'goo') ) );
			foreach ( $tabs as $id => $name ) : ?>
			<li><a href="#<?php echo $id ?>"><?php echo $name ?></a></li>
			<?php endforeach ?>
		</ul>
		
		<?php
		
		// loop to generate tabs as per functions called
		
		foreach ( $tabs as $id => $name ) {
			echo '<div id="' . $id . '" class="padding-content">';
			do_action("goo_admin_{$id}");
			echo '</div>';
		}
		
		?>

<?php function goo_admin_main_form() {

	?>
	<h3><?php _e('Design', 'goo') ?></h3>
	<table class="form-table"> 
<tr valign="top">
<th scope="row"><?php _e('Preview'); ?></th>
<td>
<?php
$background_styles = '';
if ( $bgcolor = goo_get_option('background_color') )
	$background_styles .= 'background-color: ' . $bgcolor . ';';

if ( goo_get_option('background') ) {
	// background-image URL must be single quote, see below
	$background_styles .= ' background-image: url(\'' . goo_get_option('background')  . '\');'
		. ' background-repeat: ' . goo_get_option('background_repeat', 'repeat') . ';'
		. ' background-position: top ' . goo_get_option('background_position', 'left');
}
?>
<div id="custom-background-image" style="<?php echo $background_styles; ?>"><?php // must be double quote, see above ?>
<?php if ( goo_get_option('background') ) { ?>
<img class="custom-background-image" src="<?php echo goo_get_option('background'); ?>" style="visibility:hidden;" alt="" /><br />
<img class="custom-background-image" src="<?php echo goo_get_option('background'); ?>" style="visibility:hidden;" alt="" />
<?php } ?>
</div>
<?php  ?>
</td>
</tr>	
<tr>
        <th scope="row"><p><?php _e("Custom logo image","goo"); ?><span><?php _e("Show a logo image instead of text; Upload the graphic file from your computer","goo"); ?></span></p></th>
        <td>
          <?php if(is_writable(get_upload_dir('basedir'))): ?>
           <input type="file" name="file-logo" id="file-logo" />
           <?php if(goo_get_option('logo')): ?>           
           <div class="clear-block">
            <div class="image-preview"><img src="<?php echo goo_get_option('logo'); ?>" style="padding:10px;" /></div>
			<?php echo goo_form_checkbox('goo-remove-logo', 'show', '', 'id="goo-remove-logo"') ?> Check to remove logo/image
           </div>
           <?php endif; ?>
         <?php else: ?>
         <p class="error" style="padding: 4px;"><?php printf(__("Directory %s doesn't have write permissions - can't upload!","goo"),'<strong>'.get_upload_dir('basedir').'</strong>'); ?></p><p><?php _e("Check your upload path in Settings/Misc or CHMOD this directory to 755/777.<br />Contact your host if you don't know how","goo"); ?></p>
         <?php endif; ?>
         <input type="hidden" name="logo" value="<?php echo goo_get_option('logo'); ?>">
         <input type="hidden" name="logo_size" value="<?php echo goo_get_option('logo_size'); ?>">
        </td>
       </tr>

		<tr>
        <th scope="row"><p><?php _e("Custom background image","goo"); ?><span><?php _e("Upload a new background/header image to replace the default one","goo"); ?></span></p></th>
        <td>
         <?php if(is_writable(get_upload_dir('basedir'))): ?>
           <input type="file" name="file-background" id="file-background" />
           <?php if(goo_get_option('background')): ?>
           
           <div class="clear-block">
            <div class="image-preview"><img src="<?php echo goo_get_option('background'); ?>" style="padding:10px;" /></div>
			<?php echo goo_form_checkbox('goo-remove-background', 'show', '', 'id="goo-remove-background"') ?> Check to remove background image
           </div>
           <?php endif; ?>
         <?php else: ?>
         <p class="error" style="padding: 4px;"><?php printf(__("Directory %s doesn't have write permissions - can't upload!","goo"),'<strong>'.get_upload_dir('basedir').'</strong>'); ?></p><p><?php _e("Check your upload path in Settings/Misc or CHMOD this directory to 755/777.<br />Contact your host if you don't know how","goo"); ?></p>
         <?php endif; ?>
         <input type="hidden" name="background" value="<?php echo goo_get_option('background'); ?>">
        </td>
       </tr>	
	<tr valign="top">
	<th scope="row">Color</th>
	<td><fieldset><legend class="screen-reader-text"><span>Background Color</span></legend>
	<input type="text" value="<?php echo goo_get_option('background_color') ?>" id="background-color" name="background-color" class="color">

		<div class="farbtastic"><div class="color" style="background-color: rgb(33, 255, 0);"></div>
			<div class="wheel"></div>
			<div class="overlay"></div>
			<div class="h-marker marker" style="left: 175px; top: 129px;"></div>
			<div class="sl-marker marker" style="left: 97px; top: 95px;"></div>
		</div>
	</div>
	</fieldset></td>
	</tr>
	<tr valign="top">
	<th scope="row"><label for="goo-background-repeat"><?php _e('Repeat', 'goo') ?></label></th>
	<td>	
	<?php echo goo_form_radio('goo-background-repeat', 'no-repeat', goo_get_option('background_repeat')=='no-repeat', 'id="goo-background-repeat-1"') ?> 	
	<label for="goo-layout-bg-no-repeat"><?php _e('No Repeat', 'goo') ?></label>
	<?php echo goo_form_radio('goo-background-repeat', 'repeat', goo_get_option('background_repeat')=='repeat', 'id="goo-background-repeat-2"') ?> 
	<label for="goo-layout-bg-tile"><?php _e('Tile', 'goo') ?></label>
	<?php echo goo_form_radio('goo-background-repeat', 'repeat-x', goo_get_option('background_repeat')=='repeat-x', 'id="goo-background-repeat-3"') ?> 
	<label for="goo-layout-bg-tile-horizontal"><?php _e('Tile Horizontally', 'goo') ?></label>
	<?php echo goo_form_radio('goo-background-repeat', 'repeat-y', goo_get_option('background_repeat')== 'repeat-y', 'id="goo-background-repeat-4"') ?> 
	<label for="goo-layout-bg-tile=vertical"><?php _e('Tile Vertically', 'goo') ?></label>
	
	</td>
	</tr>
	<tr valign="top">
	<th scope="row"><label for="goo-background-repeat"><?php _e('Position', 'goo') ?></label></th>
	<td>	
	<?php echo goo_form_radio('goo-background-position', 'left', goo_get_option('background_position')=='left', 'id="goo-background-position"') ?> 	
	<label for="goo-layout-bg-no-repeat"><?php _e('Left', 'goo') ?></label>
	<?php echo goo_form_radio('goo-background-position', 'center', goo_get_option('background_position')=='center', 'id="goo-background-position"') ?> 
	<label for="goo-layout-bg-tile"><?php _e('Center', 'goo') ?></label>
	<?php echo goo_form_radio('goo-background-position', 'right', goo_get_option('background_position')=='right', 'id="goo-background-position"') ?> 
	<label for="goo-layout-bg-tile-horizontal"><?php _e('Right', 'goo') ?></label>
	
	
	</td>
	</tr>		
	
	</table>
	
	<h3><?php _e('Footer Information', 'goo') ?></h3>
	<table class="form-table">
	
	<tr valign="top">
	<th scope="row"><label for="goo-footer-message"><?php _e('Footer Message', 'goo') ?></label></th>
	<td>
	<?php echo goo_form_textarea( 'goo-footer-message', form_prep(stripslashes(goo_get_option('footer_message'))), 'style="width: 70%; height: 100px;" class="code"' ) ?><br />
	<?php _e('Usually your website\'s copyright information would go here.<br /> It would be great if you could include a link to WordPress and even greater if you could include a link to the theme website. :)', 'goo') ?>
	</td>
	</tr>
	
	</table>
	
	<?php do_action('goo_admin_main_form') ?>
	
	<p class="submit">
	<input class="button-primary" type="submit" name="save" value="<?php _e('Save Changes', 'goo') ?>" />
	</p>
	<?php
}

/**
 * {@internal Missing Short Description }}
 * 
 * {@internal Missing Long Description }}
 * 
 * @hook	action	goo_admin_categories_form
 * @since	0.5.2
 */
function goo_admin_categories_form() {
	$cats = array('0' => 'None');
	foreach( get_categories('hide_empty=0') as $c ) {
		$cats[(string)$c->cat_ID] = $c->cat_name;
	}
	?>
	<h3><?php _e('Categories', 'goo') ?></h3>
	<table class="form-table">
	<tr valign="top">
	<th scope="row"><label for="goo-disable-gallery"><?php _e('Disable Featured Content', 'goo') ?></label></th>
	<td>	
	<?php echo goo_form_checkbox('goo-cat-disable-gallery', 'show', goo_get_option('disable_gallery'), 'id="goo-disable-gallery"') ?> 	
	<label for="goo-layout-display-author"><?php _e('Check to disable the gallery/featured posts at home page', 'goo') ?></label>
	<br />
	
	</td>
	</tr>
	
	<tr valign="top">
	<th scope="row"><label for="goo-cat-featured"><?php _e('Featured Category', 'goo') ?></label></th>
	<td>
	<?php echo goo_form_dropdown('goo-cat-featured', array('sticky_posts' => 'Stickied Posts', 'Available Categories' => $cats), goo_get_option('featured_cat') ); ?>
	<br /><?php _e('Articles from this category will be shown on the featured section of the index page. <br />You can also specify your stickied posts as the featured \'category\'.', 'goo') ?>
	</td>
	</tr>
		
	</table>
	
	<?php do_action('goo_admin_categories_form') ?>
	
	<p class="submit">
	<input class="button-primary" type="submit" name="save" value="<?php _e('Save Changes', 'goo') ?>" />
	</p>
	<?php	
}


/**
 * {@internal Missing Short Description }}
 * 
 * {@internal Missing Long Description }}
 * 
 * @hook	action	goo_admin_layout_form
 * @since	0.5.2
 */
function goo_admin_layout_form() {
	?>
	<h3><?php _e('Social Connection', 'goo') ?></h3>
	<table class="form-table">

	<tr valign="top">
		<th scope="row"><label for="goo-feedburner-id"><?php _e('Feedburner ID', 'goo') ?></label></th>
		<td>
		<?php echo goo_form_input(array('name' => 'goo-feedburner-id', 'id' => 'goo-feedburner-id', 'style' => 'width:50', 'value' => goo_get_option('feedburner_id') )) ?>
		<br /><?php _e('Feedburner ID', 'goo') ?>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label for="goo-facebook-prof"><?php _e('Facebook Profile URL', 'goo') ?></label></th>
		<td>
		<?php echo goo_form_input(array('name' => 'goo-facebook-prof', 'id' => 'goo-facebook-prof', 'style' => 'width:50', 'value' => goo_get_option('facebook_id') )) ?>
		<br /><?php _e('Your Facebook Profile URL', 'goo') ?>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label for="goo-twitter-id"><?php _e('Twitter ID', 'goo') ?></label></th>
		<td>
		<?php echo goo_form_input(array('name' => 'goo-twitter-id', 'id' => 'goo-twitter-id', 'style' => 'width:50', 'value' => goo_get_option('twitter_id') )) ?>
		<br /><?php _e('Your Twitter ID', 'goo') ?>
		</td>
	</tr>
<tr valign="top">

	<th scope="row"><label for="goo-about-info"><?php _e('Information', 'goo') ?></label></th>

	<td>

	<?php echo goo_form_textarea( 'goo-about-info', form_prep(stripslashes(goo_get_option('about_info'))), 'style="width: 50%; height: 100px;" class="code"' ) ?><br />

	<?php _e('Tell us more about you or your blog.', 'goo') ?>

	</td>

	</tr>	

		<tr>

        <th scope="row"><p><?php _e("Image : ","goo"); ?><span><br/><?php _e(" Upload display image/logo for information that will be displayed at the sidebar. ","goo"); ?></span></p></th>

        <td>

         <?php if(is_writable(get_upload_dir('basedir'))): ?>

           <input type="file" name="file-about-info-img" id="file-about-info-img" />
			<br/> 
			 <?php _e('Recommended size,height:80 pixels width: 80 pixels.', 'goo') ?>
           <?php if(goo_get_option('about_info_img')): ?>
			
           

           <div class="clear-block">

            <div class="image-preview"><img src="<?php echo goo_get_option('about_info_img'); ?>" style="padding:10px;" /></div>

			<?php echo goo_form_checkbox('goo-remove-about-info-img', 'show', '', 'id="goo-remove-about-info-img"') ?> Check to remove your about information image.

           </div>

           <?php endif; ?>

         <?php else: ?>

         <p class="error" style="padding: 4px;"><?php printf(__("Directory %s doesn't have write permissions - can't upload!","goo"),'<strong>'.get_upload_dir('basedir').'</strong>'); ?></p><p><?php _e("Check your upload path in Settings/Misc or CHMOD this directory to 755/777.<br />Contact your host if you don't know how","goo"); ?></p>

         <?php endif; ?>

         <input type="hidden" name="about_info_img" value="<?php echo goo_get_option('about_info_img'); ?>">

        </td>

       </tr>		
	</table>

	
		
	<h3><?php _e('Tracking Options', 'goo') ?></h3>
	<table class="form-table">
	
	<tr valign="top">
	<th scope="row"><label for="goo-google-analytics"><?php _e('Google Analytics', 'goo') ?></label></th>
	<td>
	<?php echo goo_form_textarea( 'goo-google-analytics', form_prep(stripslashes(goo_get_option('google_analytics'))), 'style="width: 50%; height: 100px;" class="code"' ) ?><br />
	<?php _e('Code provided by Google Analytics to track visitors of your website. <br/> If you still don\'t have one, visit <a href="http://www.google.com/analytics/">Analytics</a>.', 'goo') ?>
	</td>
	</tr>
	
	</table>
	
	<?php do_action('goo_admin_layout_form') ?>
	
	<p class="submit">
	<input class="button-primary" type="submit" name="save" value="<?php _e('Save Changes', 'goo') ?>" />
	</p>
	
	<?php	
}

function goo_admin_design_form() {
	?>
	<h3><?php _e('Advertisement Options', 'goo') ?></h3>
	<table class="form-table">
	
	<tr valign="top">
	<th scope="row"><label for="goo-250-ads"><?php _e('250x250 banner code ', 'goo') ?></label></th>
	<td>
	<?php echo goo_form_textarea( 'goo-250-ads', form_prep(stripslashes(goo_get_option('banner_250'))), 'style="width: 50%; height: 100px;" class="code"' ) ?><br />
	<?php _e('Your 250x250 banner code ', 'goo') ?>
	</td>
	</tr>
	<tr valign="top">
	<th scope="row"><label for="goo-468-ads-1"><?php _e('468x60 banner code - 1', 'goo') ?></label></th>
	<td>
	<?php echo goo_form_textarea( 'goo-468-ads-1', form_prep(stripslashes(goo_get_option('banner_468_60_1'))), 'style="width: 50%; height: 100px;" class="code"' ) ?><br />
	<?php _e('Your 468x60 banner code 1', 'goo') ?>
	</td>
	</tr>
	<tr valign="top">
	<th scope="row"><label for="goo-468-ads-2"><?php _e('468x60 banner code - 2', 'goo') ?></label></th>
	<td>
	<?php echo goo_form_textarea( 'goo-468-ads-2', form_prep(stripslashes(goo_get_option('banner_468_60_2'))), 'style="width: 50%; height: 100px;" class="code"' ) ?><br />
	<?php _e('Your 468x60 banner code 2', 'goo') ?>
	</td>
	</tr>
	
	
	</table>	
	<?php do_action('goo_admin_design_form');?>
	<p class="submit">
	<input class="button-primary" type="submit" name="save" value="<?php _e('Save Changes', 'goo') ?>" />
	</p>
	<?php
}

/**
 * {@internal Missing Short Description }}
 * 
 * {@internal Missing Long Description }}
 * 
 * @hook	action	goo_admin_remove_form
 * @since	0.5.2
 */
function goo_admin_reset_form() {
?>
<h3><?php _e('Revert to Default Settings', 'goo') ?></h3>
<p><?php _e('If you do screw up, you can reset the settings here.', 'goo') ?></p>
<p><?php _e('<strong>NOTE: This will erase all your settings!</strong>', 'goo') ?></p>
<p class="submit">
<input onclick="return confirm('<?php _e('Revert to default settings? This action cannot be undone!', 'goo') ?>')" class="button-secondary" type="submit" name="reset" value="<?php _e('Uninstall / Reset Edgy Ellen Theme', 'goo') ?>" />
</p>

<?php do_action('goo_admin_reset_form') ?>
	
<?php	
}		

		
		
?>