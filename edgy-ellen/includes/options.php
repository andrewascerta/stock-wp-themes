<?php
/**
 * @package Buffet
 * @subpackage API
 */

/**
 * class edgy_options
 * 
 * The edgy_options class handles the retrieval and saving of all the options in the Buffet Framework.
 * Based on Tarski's Options class.  
 * 
 * @access	public
 * @since	0.5.2
 */
class Edgy_Options {
	
	var $version;
	var $feed_url;
	
	
	var $logo;
	var $logo_size;
	var $background;
	var $background_color;
	var $background_repeat;
	var $background_position;
	var $footer_message;
	
// Gallery Option
	
	var $disable_gallery;
	var $featured_cat;
	var $featured_cat_num;

//Home options - categories

	var $home_link;
	var $comments_feed_url;
	
	var $categories;

//Miscellaneous Settings 
	
	var $feedburner_id;
	var $facebook_id;
	var $twitter_id;
	var $google_analytics;
	var $video_id;
	var $about_info;
	var $about_info_img;
//Advertisment Settings

	var $banner_250;
	var $banner_468_60_1;
	var $banner_468_60_2;	

	/**
	 * default_options() - Default Values Function
	 * 
	 * This function reset all class variables to its default values.
	 * 
	 * @access	public
	 * @since	0.5.2
	 */
	function default_options() {
		$this->version = Edgy_VERSION;
		
		$this->logo  = get_template_directory_uri() . '/images/logo.png';		
		$this->logo_size = '258x43';					
		$this->background  = get_template_directory_uri() . '/images/body_bg_pattern.png';
		$this->background_color  = '#e3f1f8';
		$this->background_repeat  = 'repeat';
		$this->background_position  = '';
		
		$this->footer_message = sprintf( __('<strong>&copy; %s</strong>. All Rights Reserved.'), get_bloginfo('name') );
			
		$this->disable_gallery = false;
		$this->featured_cat = 0;
		$this->featured_cat_num = 4;
		
		$this->feedburner_id = 'wpclassic';
		$this->facebook_id = 'wpclassic';
		$this->twitter_id = 'wpclassic';

		$this->about_info = 'Welcome to my blog.';
		
		$this->google_analytics = '';
		$this->video_id = '';

//Advertisment Settings
		
		$ad_link_pre ='<a href="http://wpclassic.com/"><img src="';
		$ad_link_suf ='"/></a>';
		
		$this->banner_250 = stripslashes($ad_link_pre . get_template_directory_uri() . '/images/250x250_ads.png' . $ad_link_suf);
		$this->banner_468_60_1 = stripslashes($ad_link_pre .  get_template_directory_uri() . '/images/468x60_ads.png' . $ad_link_suf);
		$this->banner_468_60_2 =stripslashes($ad_link_pre .  get_template_directory_uri() . '/images/468x60_ads.png' . $ad_link_suf);
	
		//$this->categories = array();
		

	}
	
	/**
	 * get_options() - Get Options Function
	 * 
	 * This function retrieves all the options from the database and sets
	 * the current class to the values.
	 * 
	 * @access	public
	 * @since	0.5.2
	 */
	function get_options() {		
		$saved_options = unserialize(get_option(THEME_ID . '_options'));
		if (!empty($saved_options) && is_object($saved_options)) {
			foreach($saved_options as $name => $value) {
				$this->$name = $value;
			}	
		}
	}
	
	/**
	 * save_options() - Save Options Function
	 * 
	 * This function is executed when the user submits the form in the admin options.
	 * It will save all form data into the current class and to the database.
	 * 
	 * @access	public
	 * @since	0.5.2
	 */
	function save_options() {
		$this->version = Edgy_VERSION;
		
		
		  if((boolean)$_POST['goo-remove-logo']==true):
		   $this->logo  = get_template_directory_uri() . '/images/logo.png';
		   $this->logo_size = '';
		  elseif($_FILES["file-logo"]["type"]):
		   $valid = is_valid_image('file-logo');
		   if($valid):
			$this->logo  =  get_upload_dir('baseurl'). "/". $_FILES["file-logo"]["name"];
			$this->logo_size = $valid;			
		   endif;
		  endif;

		    if((boolean)$_POST['goo-remove-background']==true):
		    $this->background  = '';
		  elseif($_FILES["file-background"]["type"]):
		   $valid = is_valid_image('file-background');
		   if($valid):
			$this->background  = get_upload_dir('baseurl'). "/". $_FILES["file-background"]["name"];
		   endif;
		  endif;
		$this->background_color  = '#'.(string)$_POST['background-color'];
		$this->background_repeat = (string)$_POST['goo-background-repeat'];
		$this->background_position = (string)$_POST['goo-background-position'];
		
		$this->footer_message = (string)($_POST['goo-footer-message']);
		
		$this->disable_gallery = (boolean)$_POST['goo-cat-disable-gallery'];		
		$this->featured_cat = (int)$_POST['goo-cat-featured'];

		
		$this->feedburner_id = (string)$_POST['goo-feedburner-id'];
		$this->facebook_id = (string)$_POST['goo-facebook-prof'];
		$this->twitter_id = (string)$_POST['goo-twitter-id'];
		$this->google_analytics = (string)$_POST['goo-google-analytics'];
		$this->about_info = (string)$_POST['goo-about-info'];

		if((boolean)$_POST['goo-remove-about-info-img']==true):
			$this->about_info_img  = '';
		elseif($_FILES["file-about-info-img"]["type"]):
		   $valid = is_valid_image('file-about-info-img');
		   if($valid):
			$this->about_info_img  =  get_upload_dir('baseurl'). "/". $_FILES["file-about-info-img"]["name"];
		   endif;
		endif;		
		

		//Advertisement Settings

		$this->banner_250 =stripslashes((string)$_POST['goo-250-ads']);
		$this->banner_468_60_1 = stripslashes( (string)$_POST['goo-468-ads-1']);
		$this->banner_468_60_2 = stripslashes((string)$_POST['goo-468-ads-2']);						

	}

}

/**
 * goo_flush_options() - Flush Options Function
 * 
 * Loads the options from the database and sets them to the options class instance.
 * Resets the global settings to its default settings if the option does not exist.
 * 
 * @access	public
 * @since	0.5.2
 */
function goo_flush_options() {
	global $edgy_options;
	
	$edgy_options = new Edgy_Options();
	$edgy_options->get_options();
	
	if ( !get_option(THEME_ID . '_options') ) $edgy_options->default_options();
}

/**
 * goo_update_options() - Update Options Function
 * 
 * Saves the current options class instance to the database.
 * 
 * @access	public
 * @since	0.5.2
 */
function goo_update_options() {
	global $edgy_options;
	update_option(THEME_ID . '_options', maybe_serialize($edgy_options));
}

/**
 * goo_update_option() - Update Option Function
 * 
 * Updates the option based on the ID and value given. 
 * Not to be confused with goo_update_options().
 * 
 * @access	public
 * @since	0.5.2
 * @param	string				$name	Name of the option
 * @param	mixed				$value	Value of the option
 * @param	boolean[optional]	$commit	Commit the changes to the database
 */
function goo_update_option($name, $value, $commit = true) {
	global $edgy_options;
	$edgy_options->get_options();
	
	$edgy_options->$name = $value;
	
	if ($commit) {
		goo_update_options();
		goo_flush_options();
	}
}

/**
 * goo_get_option() - Get Option Function
 * 
 * Retrieves the option based on the ID given.
 * 
 * @access	public
 * @since	0.5.2
 * @return 	mixed			Option data
 * @param	string	$name	Name of the option
 */
function goo_get_option($name) {
	global $edgy_options;
	
	if (!is_object($edgy_options) )
		goo_flush_options();
	
	return $edgy_options->$name;
}
function is_valid_image($image){
  // check mime type
  if(!eregi('image/', $_FILES[$image]['type'])):
   wp_redirect(admin_url('themes.php?page=theme-settings&error=1'));
   exit(0);
  endif;

  // check if valid image
  $imageinfo = getimagesize($_FILES[$image]['tmp_name']);
  if($imageinfo['mime'] != 'image/gif' && $imageinfo['mime'] != 'image/jpeg' && $imageinfo['mime'] != 'image/png' && isset($imageinfo)):
   wp_redirect(admin_url('themes.php?page=theme-settings&error=2'));
   exit(0);
  endif;

  list($width, $height) = $imageinfo;

  $directory = get_upload_dir('basedir').'/';
  if(!@move_uploaded_file($_FILES[$image]['tmp_name'],$directory.$_FILES[$image]["name"])):
   wp_redirect(admin_url('themes.php?page=theme-settings&error=3'));
   exit(0);
  else:
   return $width.'x'.$height;
  endif;
}
function get_upload_dir($dir) {
  $uploadpath = wp_upload_dir();
  if ($uploadpath['baseurl']=='') $uploadpath['baseurl'] = get_bloginfo('siteurl').'/wp-content/uploads';
  return $uploadpath[$dir];
}
/* End of file options.php */
/* Location: ./includes/options.php */
