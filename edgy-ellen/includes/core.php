<?php

function goo_logo(){

  $size = goo_get_option('logo_size');
  if($size) $size = 'width="'.substr($size, 0, strpos($size, "x")).'" height="'.substr($size, strpos($size, 'x')+1).'"';

  $sitename = get_bloginfo('name');
  $siteurl = get_bloginfo('url');

  $tag = (is_home() || is_front_page()) ? 'h1' : 'div';

  $output = '<'.$tag.' id="site-title">';

  if(goo_get_option('logo')) // logo image?
    $output .= '<a href="'.$siteurl.'"><img src="'.goo_get_option('logo').'" title="'.$sitename.'" '.$size.' alt="'.$sitename.'" /></a>';
  else
    $output .= '<a class="texttitle" href="'.$siteurl.'">'.$sitename.'</a>';
    $output .= '</'.$tag.'>';
  echo apply_filters('goo_logo', $output);
}

function goo_advertisement($atts){
  extract(shortcode_atts(array(
   'code' => 1,
   'align' => 'left',
   'inline' => 0
  ), $atts));
  $ad = stripslashes(goo_get_option('banner_125_'.$code));
  if(!empty($ad)):
   $ad = '<div class="ad align'.wp_specialchars($align).'">'.$ad.'</div>';
   if(!$inline) $ad = '<div class="clear-block">'.$ad.'</div>';
   return $ad;
  else:
   return '<p class="error"><strong>[ad]</strong> '.sprintf(__("Empty ad slot (#%s)!","goo"),wp_specialchars($code)).'</p>';
  endif;
}

function goo_sponsors(){
	$align='left';

	for($i=1;$i<=4;$i++){
	$ad = stripslashes(goo_get_option('banner_125_'.$i));
	 if(!empty($ad)):
	  echo '<li class="ad_align' .$align . '">'.$ad.'</li>' ;  
	 endif;
	 $align== ('left') ? $align='right':$align=left;
	}
}

function goo_flickr(){

}

function goo_doctype() {
	echo apply_filters('bf_doctype', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">');
}

function goo_featured_posts($exclude=array()) {
	wp_reset_query();	
	$featured = goo_get_option('featured_cat');
	$count = goo_get_option('featured_cat_num');
	query_posts('showposts=' . $count . '&cat=' . $featured);
	$goo_image_def = get_template_directory_uri() . '/images/thumbnail-gallery.jpg';	
?>

<div id="jquery-cycle">

	<div class="list">
	<?php while (have_posts()) : the_post(); ?>
		<div class="item append-clear">
			<a class="image" href="<?php the_permalink(); ?>" title="Permanent Link to <?php the_title_attribute(); ?>">
			<?php 
				$exclude[] = get_the_ID();
				if (has_post_thumbnail()) {
					the_post_thumbnail('featured');
				} else {
					echo '<img class="thumbnail" src="' . $goo_image_def . '" />';
				}
			?>
			</a>
	<div class="caption">
		<h3><a href="<?php the_permalink(); ?>" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
	</div>			
	</div>
	<?php endwhile; ?>
	</div>

	<div id="nav-container">
	<ul id="nav"></ul>	
	</div>
</div>

<?php
	wp_reset_query();
	remove_filter('excerpt_length','padd_theme_hook_excerpt_featured_length');
	return $exclude;
}
function goo_theme_post_thumbnail() {
	$goo_image_def = get_template_directory_uri() . '/images/thumbnail-posts.jpg';
	if (has_post_thumbnail()) {
		the_post_thumbnail();
	} else {
		echo '<img class="thumbnail" width="170" height="170" alt="Default thumbnail." src="' . $goo_image_def . '" />';
	}
}
function goo_old_post_thumbnail() {
	$goo_image_def = get_template_directory_uri() . '/images/thumbnail-old-posts.jpg';
	if (has_post_thumbnail()) {
		the_post_thumbnail('olderpost');
	} else {
		echo '<img class="old-post-img" width="50" height="50" alt="Default thumbnail." src="' . $goo_image_def . '" />';
	}
}

function goo_social_nav(){
	
	$feedburner_id = goo_get_option('feedburner_id');
	$facebook_id = goo_get_option('facebook_id');
	$twitter_id = goo_get_option('twitter_id');
?>
		<ul class="socialnet">
					<li >
				<a href="http://feedburner.google.com/fb/a/mailverify?uri=<?php echo $feedburner_id ?>&loc=en_U " class="email" title="Subscribe via email">
				</a>
			</li>
			<li >
				<a href="http://www.facebook.com/<?php echo $facebook_id ?> " class="facebook" title="Facebook Profile">
				</a>
			</li>
			<li >
				<a href="http://www.twitter.com/<?php echo $twitter_id ?>"  class="twitter" title="Twitter Profile">
				</a>			
			</li>
			

			<li >
				<a href="http://feeds.feedburner.com/<?php echo $feedburner_id; ?>"  class="rss" title="RSS Feed">				
				</a>
			</li>		</ul>

<?php	
}

function goo_fb_twit_button()
{
$twitter_id = goo_get_option('twitter_id');
?>			

				<div style="margin-right: 0px; float: right; width: 95px;">
		<a href="http://twitter.com/share" 
		   class="twitter-share-button"
		   data-count="horizontal"
		   data-url="<?php urlencode(the_permalink()); ?>"
		   data-counturl="<?php urlencode(the_permalink()); ?>"
		   data-text="<?php substr(the_title(),0,80); ?>"
		   data-via="<?php echo $twitter_id; ?>">
		   Tweet		
		</a>
				<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
			</div>
				<div style="margin: 1px 5px; float: right;">
				<a name="fb_share" type="button_count" href="http://www.facebook.com/sharer.phpu=<?php the_permalink() ?>&t=<?php the_title(); ?>" share_url="<?php the_permalink() ?>">Share</a>
				<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
				</div>


<?php
}

function social_bookmark_share ($post_permalink,$post_title,$post_excerpt){
?>
	<ul >
		<li>
			<a href="<?php echo "http://delicious.com/post?url=$post_permalink&amp;title=$post_title&amp;notes=$post_excerpt"?> "><img src="<?php echo get_template_directory_uri() ?>/images/bookmark/sb-ico-delicious.png">
		</a>
		</li>

		<li>
			<a href="<?php echo "http://digg.com/submit?phase=2&amp;url=$post_permalink&amp;title=$post_title&amp;bodytext=$post_excerpt" ?> "><img src="<?php echo get_template_directory_uri() ?>/images/bookmark/sb-ico-digg.png">
		</a>
		</li>
		<li>
			<a href="<?php echo "http://www.reddit.com/submit?url=$post_permalink&amp;title=$post_title&amp;bodytext=$post_excerpt" ?> "><img src="<?php echo get_template_directory_uri() ?>/images/bookmark/sb-ico-reddit.png">
		</a>
		</li>
		<li>
			<a href="<?php echo "http://www.stumbleupon.com/post?url=$post_permalink&amp;title=$post_title" ?> "><img src="<?php echo get_template_directory_uri() ?>/images/bookmark/sb-ico-stumble.png">
		</a>
		</li>
		<li>
			<a href="<?php echo "http://technorati.com/faves?add=$post_permalink" ?> "><img src="<?php echo get_template_directory_uri() ?>/images/bookmark/sb-ico-technorati.png">
		</a>
		</li>
	</ul>

<?php	
}
function goo_theme_widget_recent_comments($limit=5) {
	global $wpdb, $comments, $comment;

	if ( !$comments = wp_cache_get( 'recent_comments', 'widget' ) ) {
		$comments = $wpdb->get_results("SELECT * FROM $wpdb->comments WHERE comment_approved = '1' ORDER BY comment_date_gmt DESC LIMIT $limit");
		wp_cache_add( 'recent_comments', $comments, 'widget' );
	}
	echo '<ul class="comments-recent">';
	if ( $comments ) :
		foreach ( (array) $comments as $comment) :
			echo  '<li class="comments-recent">' . sprintf(__('%1$s on %2$s'), get_comment_author_link(), '<a href="'. get_comment_link($comment->comment_ID) . '">' . get_the_title($comment->comment_post_ID) . '</a>') . '</li>';
		endforeach;
	endif;
	echo '</ul>';
}

function pagination($query )
{
	
	$paged = $query->query_vars["paged"];
	
	if ( !$paged )
		$paged = 1;

	$total_pages = $query->max_num_pages;
	if ( !$total_pages )
		$total_pages = 1;

	if ( 1 == $total_pages && !$options['always_show'] )
		return;

	$request = $wp_query->request;
	$numposts = $wp_query->found_posts;

	$pages_to_show = 5;
	$larger_page_to_show = 5;
	$larger_page_multiple = 10;
	$pages_to_show_minus_1 = $pages_to_show - 1;
	$half_page_start = floor( $pages_to_show_minus_1/2 );
	$half_page_end = ceil( $pages_to_show_minus_1/2 );
	$start_page = $paged - $half_page_start;
	$total_pages = $query->max_num_pages;

	$qs = $_SERVER["QUERY_STRING"] ? "?".$_SERVER["QUERY_STRING"] : "";
	
	if ( $start_page <= 0 )
		$start_page = 1;

	$end_page = $paged + $half_page_end;

	if ( ( $end_page - $start_page ) != $pages_to_show_minus_1 )
		$end_page = $start_page + $pages_to_show_minus_1;

	if ( $end_page > $total_pages ) {
		$start_page = $total_pages - $pages_to_show_minus_1;
		$end_page = $total_pages;
	}

	if ( $start_page <= 0 )
		$start_page = 1;

		
	echo '<span class="pagenavi">';	
	$out = '';

			$out .= "<span class='pages'> Page " . $paged . ' of ' . $query->max_num_pages . "</span>";
			
			if ( $start_page >= 2 && $pages_to_show < $total_pages ) {
				$out .= '<span><a href="'.esc_url( get_pagenum_link( 1 )).'/'.$qs.'"> 1 </a></span>';
				$out .= "<span class='extend'>...</span>";
			}

			$larger_pages_array = array();
			if ( $larger_page_multiple )
				for ( $i = $larger_page_multiple; $i <= $total_pages; $i+= $larger_page_multiple )
					$larger_pages_array[] = $i;

			$larger_page_start = 0;
			foreach ( $larger_pages_array as $larger_page ) {
				if ( $larger_page < $start_page && $larger_page_start < $larger_page_to_show ) {
					$out .= '<span><a href="'.esc_url( get_pagenum_link( $larger_page )).'/'.$qs.'"> $larger_page </a></span>'; 
					$larger_page_start++;
				}
			}

		  $out .= get_previous_posts_link( "&laquo;Previous" );

			foreach ( range( $start_page, $end_page ) as $i ) {
				if ( $i == $paged ) {
					$out .=  '<span class="current"> <span class="brace">{</span> '.$i.' <span class="brace">}</span>  </span>';
				} else {
					$out .=  '<span><a href="'.esc_url( get_pagenum_link( $i )).'/'.$qs.'">'.$i.'</a></span>';
				}
			}
			$larger_page_end = 0;
			foreach ( $larger_pages_array as $larger_page ) {
				if ( $larger_page > $end_page && $larger_page_end < $larger_page_to_show ) {
					$out .= '<span><a href="'.esc_url( get_pagenum_link( $larger_page )).'/'.$qs.'"> $larger_page </a></span>'; 
					$larger_page_end++;
				}
			}

			if ( $end_page < $total_pages ) {
				$out .= "<span class='extend'>....</span>";

				$out .= '<span><a href="'.esc_url( get_pagenum_link( $total_pages )).'/'.$qs.'">' . $total_pages . '</a></span>'; 
			}
			$out .= get_next_posts_link( "Next&raquo;", $total_pages );
			echo $out;				
			echo '</span>';
}

/* End of file core.php */
/* Location: ./includes/core.php */
