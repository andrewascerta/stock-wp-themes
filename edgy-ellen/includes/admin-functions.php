<?php 

function goo_add_admin() {

    global $themename, $shortname;

	$themename = "Edgy Ellen";
	$shortname = "edgyellen";	
	
    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';
	
	$page = add_theme_page( __('Edgy Ellen Options', 'goo'), __('Edgy Ellen Options', 'goo'), 'edit_themes', 'edgy-options', 'goo_admin' );
	
	wp_enqueue_script('goo-admin-js', get_template_directory_uri() . '/includes/js/admin.js', array('jquery', 'jquery-ui-core', 'jquery-ui-tabs'), null, false);
	wp_enqueue_script('goo-color-js', get_template_directory_uri() . '/includes/js/jscolor/jscolor.js');
	wp_enqueue_style('goo-admin', get_template_directory_uri() . '/css/admin.css');	


}

	function goo_admin() {

	global $edgy_options, $wpdb;
	
	if ( isset($_GET['page']) && $_GET['page'] == 'edgy-options' ) {
		//print_r($_POST);
		
		foreach ( goo_allowed_submit_values() as $submit_value ) {
			if ( isset($_REQUEST[$submit_value]) ) {
				// Check whether form is submitted from WP admin itself
				if ( !wp_verify_nonce($_REQUEST['_wpnonce'], 'goo-admin') ) {
					die('Security Error');	
				}
				do_action("goo_admin_load_{$submit_value}");
			}	
		}
				
        }
		$nonce = wp_create_nonce('goo-admin'); // create nonce token for security
		require_once GOO_LIB . '/admin-options-ui.php';
       
}
function goo_admin_load_save() {
	global $edgy_options;
	
	$edgy_options->save_options();
	goo_update_options();
	
	echo '<div class="updated"><p>' . __('Your settings have been saved to the database.', 'goo') . '</p></div>';
}

/**
 * {@internal Missing Short Description }}
 * 
 * {@internal Missing Long Description }}
 * 
 * @ignore
 * @since	0.5.2
 */
function goo_admin_load_reset() {
	delete_option(THEME_ID . '_options');
	goo_flush_options();
	echo '<div class="updated"><p>' . __('Your settings have been reverted to the defaults.', 'goo') . '</p></div>';	
}

/**
 * {@internal Missing Short Description }}
 * 
 * {@internal Missing Long Description }}
 * 
 * @hook	filter	goo_allowed_submit_values
 * @since	0.5.2
 * @return	array	Default submit values to be validated
 */
function goo_allowed_submit_values() {
	$_default_values = array('save', 'reset');
	return apply_filters('goo_allowed_submit_values', $_default_values);
}

/**
 * {@internal Missing Short Description }}
 * 
 * {@internal Missing Long Description }}
 * 
 * @ignore
 * @since	0.5.2
 */
add_action('goo_admin_load_save', 'goo_admin_load_save');
add_action('goo_admin_load_reset', 'goo_admin_load_reset'); 
add_action('admin_menu', 'goo_add_admin'); ?>