<?php

class WPC_Walker_Nav_Menu extends Walker_Nav_Menu {
	
	/**
	 * @see Walker_Nav_Menu::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */
	var $link_counter=0;
	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;
		global $link_counter;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';
		$count;
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$item->description = trim($item->description);
		$description = ! empty( $item->description ) ? $item->description : 'Describe the page.';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before;
		if (0 == $depth) {
			$link_counter += 1;
			$item_output .= '<span class="link-number">' . sprintf("%02d",$link_counter)   . '</span>';
			$item_output .= '<span class="link-title">' . apply_filters( 'the_title', $item->title, $item->ID ) . '</span>';				
			
		} else {

			//$item_output .= '<span class="bottom-nav"><span class="left"></span>';
			$item_output .= apply_filters( 'the_title', $item->title, $item->ID );
			//$item_output .= '</span>';

		}
	
		
		$item_output .= $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;
		
		
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

}