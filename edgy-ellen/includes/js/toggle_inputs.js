function clear_comment_form(classname,value){
	jQuery(classname).focus(function() {
		if (value == jQuery(classname).val()) {
			jQuery(this).val('');
		}
	});
	jQuery(classname).blur(function() {
		if ('' == jQuery(classname).val()) {
			jQuery(this).val(value);
		}
	});
}	
$jquery2 = jQuery.noConflict();	
$jquery2(document).ready(function() {

	clear_comment_form('input#comment-author','Name');
	clear_comment_form('input#comment-email','Email');
	clear_comment_form('input#comment-url','Website');
	clear_comment_form('input#s','Search this site');
	clear_comment_form('textarea#comment-comment','Your comment');
});