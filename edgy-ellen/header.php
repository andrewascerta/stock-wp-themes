<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */ require 'includes/core-top.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php if ( is_search() || is_author() ) : ?>
<meta name="robots" content="noindex, nofollow" />
<?php endif; 
wp_enqueue_script( 'jquery');
wp_enqueue_script( 'hoverintent', get_template_directory_uri() . '/includes/js/superfish/hoverIntent.js', 'jquery' );
wp_enqueue_script( 'superfish', get_template_directory_uri() . '/includes/js/superfish/superfish.js', 'jquery' );
wp_enqueue_script( 'supersubs', get_template_directory_uri() . '/includes/js/superfish/supersubs.js', 'jquery' );
wp_enqueue_script( 'jquery.cycle', get_template_directory_uri() . '/includes/js/jquery.cycle.js', '' );




?>

<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link href='http://fonts.googleapis.com/css?family=Arimo:bold&subset=latin' rel='stylesheet' type='text/css'>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
		wp_enqueue_script( 'clear.inputs', get_template_directory_uri() . '/includes/js/toggle_inputs.js', '' );
	wp_head();
?>
<?php
$background_styles = '';
if ( $bgcolor = goo_get_option('background_color') )
	$background_styles .= 'background-color: ' . $bgcolor . ';';
if ( goo_get_option('background') ) {
	// background-image URL must be single quote, see below
	$background_styles .= ' background-image: url(\'' . goo_get_option('background')  . '\');'
		. ' background-repeat: ' . goo_get_option('background_repeat', 'repeat') . ';'
		. ' background-position: top ' . goo_get_option('background_position', 'left');
}
echo '<style type="text/css"> body {';
echo $background_styles . '}'.PHP_EOL;
if ( goo_get_option('logo_size') ) {
$mastheadheight = explode('x',goo_get_option('logo_size'));
$mastheadheight = $mastheadheight[1];

}
?>

</style>
<style type="text/css">
.slideshow { height: 232px; width: 100%; margin: auto }
.slideshow img { padding: 15px; border: 1px solid #ccc; background-color: #eee; }
</style>

<script type="text/javascript">

function goonavigation(){
$jquery = jQuery.noConflict();
	$jquery(function(){
		$jquery('ul#menu-nav_menu').superfish(
			{
			animation: {height:'show'},
            delay:     2000
		}
		
		);
	});
	$jquery('div.menu > ul').superfish(	{
			animation: {height:'show'},
            delay:     2000
		}); 
    
	
}	
function gooslideshow() {
$jquery = jQuery.noConflict();

$jquery('#jquery-cycle div.list').cycle({ 
    timeout: 15000, 
    pager:  '#nav', 
    pagerAnchorBuilder: function(idx, slide) { 
        return '<li><a href="#"><img src="' + '<?php bloginfo('template_url'); ?>/images/cycle/feat-li.png" /></a></li>'; 
    } 
});

	$jquery.fn.cycle.updateActivePagerLink = function(pager, currSlideIndex) {
		$jquery(pager).find('li').removeClass('activeSlide')
			.filter('li:eq('+currSlideIndex+')').addClass('activeSlide');
	};

 
}	
jQuery(document).ready(function() {
jQuery.noConflict();
	goonavigation();
	gooslideshow();

});

</script>

</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
	<div id="header">
		<div id="masthead">
			<div id="branding" role="banner">
				<?php goo_logo(); ?>
				
			</div><!-- #branding -->
			
						
				<div id="access" role="navigation">
			  <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
				<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a></div>				
				<?php wp_nav_menu(array( 'container_class' => 'menu-header', 'theme_location' => null,'walker'=>new WPC_Walker_Nav_Menu() ) ); ?>				
				<?php //goo_nav(); ?>		
				</div><!-- #access -->
		</div><!-- #masthead -->

	</div><!-- #header -->
	
	<div id="main">
