<div id="colophon">

			<div role="complementary" id="footer-widget-area">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 1') ) : ?>
				<div id="first" class="widget-area box-recent-comments">					
					<h3 class="widget-title-footer">Recent Comments</h3>
					<div class="interior">						
						<?php goo_theme_widget_recent_comments(); ?>						
					</div>
				</div>
			<?php endif; ?>
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 2') ) : ?>
				<div id="fourth" class="widget-area box-archives">
					<h3 class="widget-title-footer">Archives</h3>
					<div class="interior">						
						<ul>
						<?php wp_get_archives( 'type=monthly' ); ?>
						</ul>
					</div>
				</div>
			<?php endif; ?>

			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 3') ) : ?>
				<div id="third" class="widget-area box-blogroll">
					<h3 class="widget-title-footer">Blogroll</h3>
					<div class="interior">	
						<ul>
						<?php wp_tag_cloud(); ?>
						</ul>
					</div>
				</div>
			<?php endif; ?>

			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 4') ) : ?>
				<div id="second" class="widget-area box-recent-comments">
					<h3 class="widget-title-footer">Twitter Status</h3>
					<div class="interior">						
						<?php $twitter_id = goo_get_option('twitter_id');							
							echo Goo_Twitter::get_messages($twitter_id,3,true);
						?>
					</div>
				</div>
			<?php endif; ?>			
			</div>
		</div><!-- #colophon -->