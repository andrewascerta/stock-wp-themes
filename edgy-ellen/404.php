<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<div id="container">
		<div id="content" role="main">

			
				<h1 class="page-title"><?php _e( 'Not Found', 'twentyten' ); ?></h1>
				<div class="post-group">
					<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'twentyten' ); ?></p>
					<?php //get_search_form(); ?>
				</div><!-- .entry-content -->
			

		</div><!-- #content -->
		<?php get_sidebar(); ?>
	</div><!-- #container -->
	<script type="text/javascript">
		// focus on search field after it has loaded
		document.getElementById('s') && document.getElementById('s').focus();
	</script>

<?php get_footer(); ?>