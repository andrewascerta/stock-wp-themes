<?php get_header(); ?>

		<header class="page-header left-12col">
		<h1 class="banner"><?php printf( __( 'Search results for: %s', 'extricate' ), '<em>' . get_search_query() . '</em>' ); ?></h1>
		</header>

		<section id="primary" class="left-<?php $options = get_option('extricate_theme_options'); if ($options['columnwidth'] == "thirds") { ?>08<?php } else { ?>09<?php } ?>col" role="main">

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					
					<?php get_template_part( 'content', 'search' ); ?>

				<?php endwhile; ?>
				
				<?php /* Display navigation to next/previous pages when applicable */ ?>
				<?php if (  $wp_query->max_num_pages > 1 ) : ?>
					<nav id="older-newer">
						<div class="older"><?php next_posts_link( __( '&larr; Older posts', 'extricate' ) ); ?></div>
						<div class="newer"><?php previous_posts_link( __( 'Newer posts &rarr;', 'extricate' ) ); ?></div>
					</nav><!-- end older-newer -->
				<?php endif; ?>	

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'extricate' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'extricate' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

		</section><!-- end primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>