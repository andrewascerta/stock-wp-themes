<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( is_search() ) : // Search page excerpts ?>

	<div class="entry-summary">

    <header class="entry-header">
		<h1><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'extricate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<p class="hang-right"><a href="<?php the_permalink() ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'extricate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_time('F jS, Y') ?></a></p>
	</header><!-- end entry-header -->

		<?php the_excerpt( __( 'Continue reading &rarr;', 'extricate' ) ); ?>
	</div><!-- .entry-summary -->

	<?php else : ?>

	<header class="entry-header">
		<h1><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'extricate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<p class="hang-right"><a href="<?php the_permalink() ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'extricate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_time('F jS, Y') ?></a></p>
	</header><!-- end entry-header -->

	<?php if ( 'post' == $post->post_type ) : ?>
	<div class="entry-meta">
	<ul>
		<?php $options = get_option('extricate_theme_options'); if ($options['removeauthor'] == "1") { ?>
		<!-- no author -->
		<?php } else { ?>
		<li>By: <?php printf( __( '<a href="%1$s" title="%2$s">%3$s</a>', 'extricate' ), get_author_posts_url( get_the_author_meta( 'ID' ) ), sprintf( esc_attr__( 'View all posts by %s', 'extricate' ), get_the_author() ), get_the_author() ); ?></li>
		<?php } ?>
		<li>Filed under: <?php the_category(', ') ?></li>
		<?php the_tags('<li>Tagged with: ', ', ', '</li>'); ?>
	</ul>
	</div><!-- end entry-meta -->
	<?php endif; ?>

    <div class="entry-content">
		<?php the_content( __( 'Continue reading &rarr;', 'extricate' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'extricate' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->

	<?php if ( 'post' == $post->post_type ) : ?>
	<footer class="entry-extras">
	<?php $options = get_option('extricate_theme_options'); if ($options['sharinglinks'] == "1") { include (TEMPLATEPATH . '/assets/inc/social.php' ); } else { ?><!-- no social sharing links --><?php } ?>
	<p><?php comments_popup_link( __( 'Leave a comment', 'extricate' ), __( '1 Comment', 'extricate' ), __( '% Comments', 'extricate' ) ); ?>
	<?php edit_post_link( __( 'Edit', 'extricate' ), '| <span class="edit-link">', '</span>' ); ?></p>
	</footer><!-- end entry-extras -->
	<?php endif; ?>

	<?php endif; ?>

</article><!-- end post-<?php the_ID(); ?> -->