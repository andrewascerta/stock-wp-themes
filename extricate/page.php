<?php get_header(); ?>

		<div id="primary" class="left-<?php $options = get_option('extricate_theme_options'); if ($options['columnwidth'] == "thirds") { ?>08<?php } else { ?>09<?php } ?>col" role="main">

			<?php the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

			<?php comments_template( '', true ); ?>

		</div><!-- end primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>