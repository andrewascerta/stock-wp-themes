<?php get_header(); ?>

	<section id="primary" class="left-<?php $options = get_option('extricate_theme_options'); if ($options['columnwidth'] == "thirds") { ?>08<?php } else { ?>09<?php } ?>col" role="main">

		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

				<nav id="older-newer">
					<div class="older"><?php previous_post_link('%link', '&larr; Previously: <strong>%title</strong>'); ?></div>
					<div class="newer"><?php next_post_link('%link', 'Next: <strong>%title</strong> &rarr;'); ?></div>
				</nav> <!-- end older-newer -->

			<?php comments_template( '', true ); ?>

		<?php endwhile; // end of the loop. ?>

	</section><!-- end primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>