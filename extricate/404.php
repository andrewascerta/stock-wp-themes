<?php get_header(); ?>

	<section id="primary" class="left-12col" role="main">

		<article id="post-0" class="error404">
			<header class="page-header">
			<h1 class="banner"><?php _e( 'Uh-oh, it\'s a 404 error &#8212; Page not found', 'extricate' ); ?></h1>
			</header>

				<div class="entry-content">
					<p style="text-align:center;"><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching (up there, top right), or one of the links below, can help.', 'extricate' ); ?></p>

					<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>

					<div class="widget">
						<h2><?php _e( 'Most Used Categories', 'extricate' ); ?></h2>
						<ul>
						<?php wp_list_categories( array( 'orderby' => 'count', 'order' => 'DESC', 'show_count' => 'TRUE', 'title_li' => '', 'number' => '10' ) ); ?>
						</ul>
					</div>

					<?php
					$archive_content = '<p>' . sprintf( __( 'Take a look in the monthly archives.', 'extricate' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
					?>

					<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>

				</div><!-- end entry-content -->
			</article><!-- end post-0 -->

	</section><!-- end primary -->

<?php get_footer(); ?>