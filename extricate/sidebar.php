		<div id="sidebar" class="widget-area left-<?php $options = get_option('extricate_theme_options'); if ($options['columnwidth'] == "thirds") { ?>04<?php } else { ?>03<?php } ?>col" role="complementary">

		<?php if ( ! dynamic_sidebar( 'sidebar' ) ) : ?>

		<aside class="widget">
		<h1>Sidebar</h1>

	    <p>Here's the sidebar.</p>
	    <p>To make stuff appear here, go to <strong>Appearance &raquo; Widgets</strong> in your WordPress admin section and drag'n'drop.</p>
		</aside>

		<?php endif; // end sidebar widget area ?>
		</div><!-- #secondary .widget-area -->