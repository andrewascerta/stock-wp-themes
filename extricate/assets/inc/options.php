<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
	register_setting( 'extricate_options', 'extricate_theme_options', 'theme_options_validate' );
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
	add_theme_page( __( 'Extricate Options' ), __( 'Extricate Options' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}

/**
 * Create arrays for our select and radio options
 */
$select_options = array(
	'helvetica' => array(
		'value' =>	'helvetica',
		'label' => __( 'Helvetica' )
	),
	'georgia' => array(
		'value' => 'georgia',
		'label' => __( 'Georgia' )
	),
	'verdana' => array(
		'value' => 'verdana',
		'label' => __( 'Verdana' )
	),
	'geneva' => array(
		'value' => 'geneva',
		'label' => __( 'Geneva' )
	)
);

$radio_options = array(
	'quarters' => array(
		'value' => 'quarters',
		'label' => __( 'Quarters' )
	),
	'thirds' => array(
		'value' => 'thirds',
		'label' => __( 'Thirds' )
	)
);

/**
 * Create the options page
 */
function theme_options_do_page() {
	global $select_options, $radio_options;

	if ( ! isset( $_REQUEST['updated'] ) )
		$_REQUEST['updated'] = false;

	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . get_current_theme() . __( ' Theme Options' ) . "</h2>"; ?>

		<?php if ( false !== $_REQUEST['updated'] ) : ?>
		<div class="updated fade"><p><strong><?php _e( 'Options saved' ); ?></strong></p></div>
		<?php endif; ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'extricate_options' ); ?>
			<?php $options = get_option( 'extricate_theme_options' ); ?>


            <h3 style="margin: 30px 0 0 0; padding: 10px 0 0 0; border-top: 1px dotted #ccc;">Basic layout</h3>

			<table class="form-table">

				<?php
				/**
				 * columnwidth
				 */
				?>
				<tr valign="top"><th scope="row"><?php _e( 'Main column widths:' ); ?></th>
					<td>
						<fieldset><legend class="screen-reader-text"><span><?php _e( 'columnwidth' ); ?></span></legend>
						<?php
							if ( ! isset( $checked ) )
								$checked = '';
							foreach ( $radio_options as $option ) {
								$radio_setting = $options['columnwidth'];

								if ( '' != $radio_setting ) {
									if ( $options['columnwidth'] == $option['value'] ) {
										$checked = "checked=\"checked\"";
									} else {
										$checked = '';
									}
								}
								?>
								<label class="description"><input type="radio" name="extricate_theme_options[columnwidth]" value="<?php esc_attr_e( $option['value'] ); ?>" <?php echo $checked; ?> /> <?php echo $option['label']; ?></label><br />
								<?php
							}
						?>
						</fieldset>
                        <?php _e( 'Set the width of your website\'s columns in quarters or thirds. The default is quarters. [LINK TO BLOG POST]' ); ?>
                        
					</td>
				</tr>

			</table>
            
            <h3 style="margin: 30px 0 0 0; padding: 10px 0 0 0; border-top: 1px dotted #ccc;">Post options</h3>

			<table class="form-table"> 
                
                <?php
				/**
				 * sharinglinks
				 */
				?>
				<tr valign="top"><th scope="row"><?php _e( 'Enable social sharing icons?' ); ?></th>
					<td>
						<input id="extricate_theme_options[sharinglinks]" name="extricate_theme_options[sharinglinks]" type="checkbox" value="1" <?php checked( '1', $options['sharinglinks'] ); ?> />
						<label class="description" for="extricate_theme_options[sharinglinks]"><?php _e( 'Add icons to your posts to enable them to be shared on Twitter, Facebook, Tumblr, Reddit, StumbleUpon, Digg and Delicious.' ); ?></label>
					</td>
				</tr>
                
                <?php
				/**
				 * removeauthor
				 */
				?>
				<tr valign="top"><th scope="row"><?php _e( 'Remove author credit?' ); ?></th>
					<td>
						<input id="extricate_theme_options[removeauthor]" name="extricate_theme_options[removeauthor]" type="checkbox" value="1" <?php checked( '1', $options['removeauthor'] ); ?> />
						<label class="description" for="extricate_theme_options[removeauthor]"><?php _e( 'Remove the author credit beside each post.' ); ?></label>
					</td>
				</tr>
                
			</table>
            
            <h3 style="margin: 30px 0 0 0; padding: 10px 0 0 0; border-top: 1px dotted #ccc;">Background colours:</h3>

			<table class="form-table"> 
                
                 <?php
				/**
				 * bodycolour
				 */
				?>
				<tr valign="top"><th scope="row"><?php _e( 'Main body colour:' ); ?></th>
					<td>
						<strong>#</strong><input id="extricate_theme_options[bodycolour]" class="regular-text" type="text" name="extricate_theme_options[bodycolour]" value="<?php esc_attr_e( $options['bodycolour'] ); ?>"  style="width: 50px;" />
						<br /><label class="description" for="extricate_theme_options[bodycolour]"><?php _e( 'Set the main body colour of your site to any hexadecimal colour value &#8212; you can <a href="http://www.colorpicker.com/">pick one here</a>.<br /> The default is #FFFFFF; the purest snow white.' ); ?></label>
					</td>
				</tr>
                
                <?php
				/**
				 * bgcolour
				 */
				?>
				<tr valign="top"><th scope="row"><?php _e( 'Background colour:' ); ?></th>
					<td>
						<strong>#</strong><input id="extricate_theme_options[bgcolour]" class="regular-text" type="text" name="extricate_theme_options[bgcolour]" value="<?php esc_attr_e( $options['bgcolour'] ); ?>"  style="width: 50px;" />
						<br /><label class="description" for="extricate_theme_options[bgcolour]"><?php _e( 'Set the background colour of the header and footer of your site to any hexadecimal colour value &#8212; you can <a href="http://www.colorpicker.com/">pick one here</a>.<br /> The default is #EEEEEE, a nice light grey.' ); ?></label>
					</td>
				</tr>
                
                </table>
            
            <h3 style="margin: 30px 0 0 0; padding: 10px 0 0 0; border-top: 1px dotted #ccc; color: #999; font-weight: normal;">Background image (<a href="http://www.snoother.com/extricate/">Extricate Plus feature</a>)</h3>

			<table class="form-table"> 
				
				<?php
				/**
				 * bgimg
				 */
				?>
				<tr valign="top"><th scope="row" style="color: #999;"><?php _e( 'Header background image:' ); ?></th>
					<td>
						<input id="extricate_theme_options[bgimg]" class="regular-text" type="text" name="extricate_theme_options[bgimg]" value="<?php esc_attr_e( $options['bgimg'] ); ?>" disabled="disabled" />
						<br /><label class="description" for="extricate_theme_options[bgimg]"><?php _e( 'Enter the URL of an image file to be used as the site header background image, if you want one (if not, just leave it blank). <br />This works best with a long, thin image strip of 120 pixels in height, but it\'s up to you. <br />You should host this file locally if you can, but if not, <a href="http://imgur.com/">Imgur.com</a> can be a handy stopgap way of doing this, as it lets you upload, crop and resize pictures for free.' ); ?></label>
					</td>
				</tr>

			</table>
            
            <h3 style="margin: 30px 0 0 0; padding: 10px 0 0 0; border-top: 1px dotted #ccc; color: #999; font-weight: normal;">Site navigation options (<a href="http://www.snoother.com/extricate/">Extricate Plus feature</a>)</h3>


			<table class="form-table"> 
				
				<?php
				/**
				 * disablenav
				 */
				?>
				<tr valign="top"><th scope="row" style="color: #999;"><?php _e( 'Remove main navigation?' ); ?></th>
					<td>
						<input id="extricate_theme_options[disablenav]" name="extricate_theme_options[disablenav]" type="checkbox" value="1" <?php checked( '1', $options['disablenav'] ); ?> disabled="disabled" />
						<label class="description" for="extricate_theme_options[disablenav]"><?php _e( 'Remove the entire main navigation bar for a hyper-minimalist look.' ); ?></label>
					</td>
				</tr>
                
                <?php
				/**
				 * disablesearch
				 */
				?>
				<tr valign="top"><th scope="row" style="color: #999;"><?php _e( 'Remove search form?' ); ?></th>
					<td>
						<input id="extricate_theme_options[disablesearch]" name="extricate_theme_options[disablesearch]" type="checkbox" value="1" <?php checked( '1', $options['disablesearch'] ); ?> disabled="disabled" />
						<label class="description" for="extricate_theme_options[disablesearch]"><?php _e( 'Remove the search form from the main navigation bar.' ); ?></label>
					</td>
				</tr>

			</table>
            
            <h3 style="margin: 30px 0 0 0; padding: 10px 0 0 0; border-top: 1px dotted #ccc; color: #999; font-weight: normal;">Link colours (<a href="http://www.snoother.com/extricate/">Extricate Plus feature</a>)</h3>

			<table class="form-table"> 

				<?php
				/**
				 * linkcolour
				 */
				?>
				<tr valign="top"><th scope="row" style="color: #999;"><?php _e( '<strong>Main link colour:</strong>' ); ?></th>
					<td>
						<strong>#</strong><input id="extricate_theme_options[linkcolour]" class="regular-text" type="text" name="extricate_theme_options[linkcolour]" value="<?php esc_attr_e( $options['linkcolour'] ); ?>" style="width: 50px;" disabled="disabled" />
						<label class="description" for="extricate_theme_options[linkcolour]"><?php _e( 'Set the main link colour for the site &#8212; <a href="http://www.colorpicker.com/">pick one here</a>. The default is #4064b6, an elegant mid-blue.' ); ?></label>
					</td>
				</tr>
                
                <?php
				/**
				 * linkcolourvisited
				 */
				?>
				<tr valign="top"><th scope="row" style="color: #999;"><?php _e( 'Link colour (visited links):' ); ?></th>
					<td>
						<strong>#</strong><input id="extricate_theme_options[linkcolourvisited]" class="regular-text" type="text" name="extricate_theme_options[linkcolourvisited]" value="<?php esc_attr_e( $options['linkcolourvisited'] ); ?>" style="width: 50px;" disabled="disabled" />
						<label class="description" for="extricate_theme_options[linkcolourvisited]"><?php _e( 'Set the link colour for visited links in the site &#8212; <a href="http://www.colorpicker.com/">pick one here</a>.' ); ?></label>
					</td>
				</tr>
                
                <?php
				/**
				 * linkcolourhover
				 */
				?>
				<tr valign="top"><th scope="row" style="color: #999;"><?php _e( 'Link colour (hover):' ); ?></th>
					<td>
						<strong>#</strong><input id="extricate_theme_options[linkcolourhover]" class="regular-text" type="text" name="extricate_theme_options[linkcolourhover]" value="<?php esc_attr_e( $options['linkcolourhover'] ); ?>" style="width: 50px;" disabled="disabled" />
						<label class="description" for="extricate_theme_options[linkcolourhover]"><?php _e( 'Set the colour for when links are hovered over by a mouse pointer &#8212; <a href="http://www.colorpicker.com/">pick one here</a>.' ); ?></label>
					</td>
				</tr>
                
                <?php
				/**
				 * linkcolouractive
				 */
				?>
				<tr valign="top"><th scope="row" style="color: #999;"><?php _e( 'Link colour (active):' ); ?></th>
					<td>
						<strong>#</strong><input id="extricate_theme_options[linkcolouractive]" class="regular-text" type="text" name="extricate_theme_options[linkcolouractive]" value="<?php esc_attr_e( $options['linkcolouractive'] ); ?>" style="width: 50px;" disabled="disabled" />
						<label class="description" for="extricate_theme_options[linkcolouractive]"><?php _e( 'Set the colour for when links are clicked by a mouse pointer &#8212; <a href="http://www.colorpicker.com/">pick one here</a>.' ); ?></label>
					</td>
				</tr>
                
			</table>
            
            <h3 style="margin: 30px 0 0 0; padding: 10px 0 0 0; border-top: 1px dotted #ccc; color: #999; font-weight: normal;">Site typeface (<a href="http://www.snoother.com/extricate/">Extricate Plus feature</a>)</h3>

			<table class="form-table"> 

				<?php
				/**
				 * typeface
				 */
				?>
				<tr valign="top"><th scope="row" style="color: #999;"><?php _e( 'Set the site typeface:' ); ?></th>
					<td>
						<select name="extricate_theme_options[typeface]" disabled="disabled">
							<?php
								$selected = $options['typeface'];
								$p = '';
								$r = '';

								foreach ( $select_options as $option ) {
									$label = $option['label'];
									if ( $selected == $option['value'] ) // Make default first in list
										$p = "\n\t<option style=\"padding-right: 10px;\" selected='selected' value='" . esc_attr( $option['value'] ) . "'>$label</option>";
									else
										$r .= "\n\t<option style=\"padding-right: 10px;\" value='" . esc_attr( $option['value'] ) . "'>$label</option>";
								}
								echo $p . $r;
							?>
						</select>
						<label class="description" for="extricate_theme_options[typeface]"><?php _e( 'Choose a typeface for the posts and pages of the site. The default is Helvetica (which will <a href="http://www.ms-studio.com/articles.html">appear as Arial</a> on most PCs).' ); ?></label>
					</td>
				</tr>
                
                </table>
            
            <h3 style="margin: 30px 0 0 0; padding: 10px 0 0 0; border-top: 1px dotted #ccc; color: #999; font-weight: normal;">Google Analytics (<a href="http://www.snoother.com/extricate/">Extricate Plus feature</a>)</h3>

			<table class="form-table"> 

                
                <?php
				/**
				 * analytics
				 */
				?>
				<tr valign="top"><th scope="row" style="color: #999;"><?php _e( 'Add Google Analytics:' ); ?></th>
					<td>
						<input id="extricate_theme_options[analytics]" class="regular-text" type="text" name="extricate_theme_options[analytics]" value="<?php esc_attr_e( $options['analytics'] ); ?>" disabled="disabled" />
						<br /><label class="description" for="extricate_theme_options[analytics]"><?php _e( 'Add your Google Analytics ID here.<br /> <strong>PLEASE NOTE: Do <em>not</em> enter the full tracking code here.</strong><br /> All you need to enter is the ID of your Google analytics account, which usually looks something like this &#8212; <code>UA-21668404-2</code> &#8212; the rest of the tracking code will be added automagically.' ); ?></label>
					</td>
				</tr>
                
                
			</table>
                        
			<p class="submit" style="margin: 30px 0 30px 0; padding: 10px 0 0 0; border-top: 1px dotted #ccc;">
				<input type="submit" class="button-primary" value="<?php _e( 'Save Options' ); ?>" />
			</p>
		</form>
	</div>
	<?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function theme_options_validate( $input ) {
	global $select_options, $radio_options;

	// Our checkbox value is either 0 or 1
	if ( ! isset( $input['disablenav'] ) )
		$input['disablenav'] = null;
	$input['disablenav'] = ( $input['disablenav'] == 1 ? 1 : 0 );

	// Our checkbox value is either 0 or 1
	if ( ! isset( $input['disablesearch'] ) )
		$input['disablesearch'] = null;
	$input['disablesearch'] = ( $input['disablesearch'] == 1 ? 1 : 0 );

	// Our checkbox value is either 0 or 1
	if ( ! isset( $input['sharinglinks'] ) )
		$input['sharinglinks'] = null;
	$input['sharinglinks'] = ( $input['sharinglinks'] == 1 ? 1 : 0 );

	// Our checkbox value is either 0 or 1
	if ( ! isset( $input['removeauthor'] ) )
		$input['removeauthor'] = null;
	$input['removeauthor'] = ( $input['removeauthor'] == 1 ? 1 : 0 );

	// Say our text option must be safe text with no HTML tags
	$input['bgcolour'] = wp_filter_nohtml_kses( $input['bgcolour'] );

	// Say our text option must be safe text with no HTML tags
	$input['bgimg'] = wp_filter_nohtml_kses( $input['bgimg'] );

	// Say our text option must be safe text with no HTML tags
	$input['linkcolour'] = wp_filter_nohtml_kses( $input['linkcolour'] );

	// Say our text option must be safe text with no HTML tags
	$input['linkcolourvisited'] = wp_filter_nohtml_kses( $input['linkcolourvisited'] );

	// Say our text option must be safe text with no HTML tags
	$input['linkcolourhover'] = wp_filter_nohtml_kses( $input['linkcolourhover'] );

	// Say our text option must be safe text with no HTML tags
	$input['linkcolouractive'] = wp_filter_nohtml_kses( $input['linkcolouractive'] );

	// Our select option must actually be in our array of select options
	if ( ! array_key_exists( $input['typeface'], $select_options ) )
		$input['typeface'] = null;

	// Our radio option must actually be in our array of radio options
	if ( ! isset( $input['columnwidth'] ) )
		$input['columnwidth'] = null;
	if ( ! array_key_exists( $input['columnwidth'], $radio_options ) )
		$input['columnwidth'] = null;

	// Say our text option must be safe text with no HTML tags
	$input['analytics'] = wp_filter_nohtml_kses( $input['analytics'] );

	return $input;
}

// adapted from http://planetozh.com/blog/2009/05/handling-plugins-options-in-wordpress-28-with-register_setting/