<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	// Add the blog name.
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'extricate' ), max( $paged, $page ) );
	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<style type="text/css" media="screen">
<?php 
$options = get_option('extricate_theme_options');
if( isset( $options['linkcolour'] ) && ( !empty( $options['linkcolour'] ) ) )   
    printf( "a, a:link {color: #%s;}", $options['linkcolour'] );

if( isset( $options['linkcolourvisited'] ) && ( !empty( $options['linkcolourvisited'] ) ) )   
    printf( "a:visited {color: #%s;}", $options['linkcolourvisited'] );

if( isset( $options['linkcolourhover'] ) && ( !empty( $options['linkcolourhover'] ) ) )   
    printf( "a:hover {background: #fff; color: #%s;}", $options['linkcolourhover'] );

if( isset( $options['linkcolouractive'] ) && ( !empty( $options['linkcolouractive'] ) ) )   
    printf( "a:active {color: #%s;}", $options['linkcolouractive'] );

if( isset( $options['bodycolour'] ) && ( !empty( $options['bodycolour'] ) ) )   
    printf( "#wrap-main {background: #%s;}", $options['bodycolour'] );

if( isset( $options['bgcolour'] ) && ( !empty( $options['bgcolour'] ) ) )   
    printf( "body {background: #%s;}", $options['bgcolour'] );

if( isset( $options['bgimg'] ) && ( !empty( $options['bgimg'] ) ) )   
    printf( "body {background-image: url(%s); background-position: center top; background-repeat: repeat-x;}", $options['bgimg'] );
?>
</style>

<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed">
	<header id="top" role="banner">
			<hgroup>
				<h1 id="site-title"><span><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span></h1>
				<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
			</hgroup>
	</header><!-- end top -->

<?php $options = get_option('extricate_theme_options'); if ($options['disablenav'] == "1") { ?>
<div id="no-nav"><!-- nonav --></div>
<?php } else { ?>
	<header id="wrap-nav">
		<nav id="mainnav" role="navigation">
			<div class="skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'extricate' ); ?>"><?php _e( 'Skip to content', 'extricate' ); ?></a></div>
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			<?php $options = get_option('extricate_theme_options'); if ($options['disablesearch'] == "1") { ?><!-- no search --><?php } else { include (TEMPLATEPATH . '/assets/inc/searchform.php' ); } ?>
		</nav><!-- end mainnav -->
	</header><!-- end wrap-nav -->
<?php } ?>

	<div id="wrap-main"> <div id="main" class="<?php $options = get_option('extricate_theme_options'); echo $options['typeface']; ?>">