<?php
/**
 * Make theme available for translation
 */
load_theme_textdomain( 'extricate', TEMPLATEPATH . '/languages' );

$locale = get_locale();
$locale_file = TEMPLATEPATH . "/languages/$locale.php";
if ( is_readable( $locale_file ) )
	require_once( $locale_file );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

/**
 * This theme uses wp_nav_menu() in one location.
 */
register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'extricate' ),
) );

/**
 * Add theme support for menus.
 */
if (function_exists('add_theme_support')) {
	add_theme_support('menus');
}

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function extricate_page_menu_args($args) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'extricate_page_menu_args' );

/**
 * Add default posts and comments RSS feed links to head
 */
add_theme_support( 'automatic-feed-links' );

/**
 * Load jQuery
 */
if ( !is_admin() ) {
	wp_deregister_script('jquery');
	wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"), false);
	wp_enqueue_script('jquery');
}

/**
 * Add support for the Aside and Gallery Post Formats
 */
add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function extricate_widgets_init() {
	register_sidebar( array (
		'name' => __( 'Site sidebar', 'extricate' ),
		'id' => 'sidebar',
		'description' => __( 'Your sidebar content', 'extricate' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h1>',
		'after_title' => '</h1>',
	) );

	register_sidebar( array (
		'name' => __( 'Site footer', 'extricate' ),
		'id' => 'site-footer',
		'description' => __( 'Your site footer content', 'extricate' ),
		'before_widget' => '<div id="%1$s" class="left-03col %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h1>',
		'after_title' => '</h1>',
	) );	
}
add_action( 'init', 'extricate_widgets_init' );

/**
 * Clean up <head> -- remove recent comments filter
 */

function extricate_remove_recent_comments_style() {  
	global $wp_widget_factory;  
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );  
}
add_action( 'widgets_init', 'extricate_remove_recent_comments_style' );


/**
 * Clean up <head> -- remove other stuff
 */

function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

/**
 * Load options
 */

require_once ( get_stylesheet_directory() . '/assets/inc/options.php' );

/**
 * Google analytics
 */

function __analytics_head()
{
    $options = get_option( 'extricate_theme_options' );
    if ( !empty( $options['analytics'] ) ) :
    ?>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', '<?php echo esc_js( $options['analytics'] ) ; ?>']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
    <?php
    endif;
}
add_action( 'wp_head', '__analytics_head', 100 );

?>