<?php get_header(); ?>

				<header class="page-header left-12col">
					<h1 class="banner">
						<?php if ( is_day() ) : ?>
							<?php printf( __( 'Daily Archives: <span>%s</span>', 'extricate' ), get_the_date() ); ?>
						<?php elseif ( is_month() ) : ?>
							<?php printf( __( 'Monthly Archives: <span>%s</span>', 'extricate' ), get_the_date( 'F Y' ) ); ?>
						<?php elseif ( is_year() ) : ?>
							<?php printf( __( 'Yearly Archives: <span>%s</span>', 'extricate' ), get_the_date( 'Y' ) ); ?>
						<?php else : ?>
							<?php _e( 'Blog Archives', 'extricate' ); ?>
						<?php endif; ?>
					</h1>
				</header>

		<section id="primary" class="left-<?php $options = get_option('extricate_theme_options'); if ($options['columnwidth'] == "thirds") { ?>08<?php } else { ?>09<?php } ?>col" role="main">

				<?php the_post(); ?>

				<?php rewind_posts(); ?>
				
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					
					<?php get_template_part( 'content', get_post_format() ); ?>

				<?php endwhile; ?>
				
				<?php /* Display navigation to next/previous pages when applicable */ ?>
				<?php if (  $wp_query->max_num_pages > 1 ) : ?>
					<nav id="older-newer">
						<div class="older"><?php next_posts_link( __( '&larr; Older posts', 'extricate' ) ); ?></div>
						<div class="newer"><?php previous_posts_link( __( 'Newer posts &rarr;', 'extricate' ) ); ?></div>
					</nav><!-- end older-newer -->
				<?php endif; ?>				

		</section><!-- end primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>