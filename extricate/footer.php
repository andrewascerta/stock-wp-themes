	</div><!-- end main -->
	</div><!-- end wrap-main -->

	<footer id="colophon" role="contentinfo">
<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('site-footer')) : else : ?>

	<!-- Content below only appears when there are no active Footer widgets -->

	<div class="left-03col">
	<h1>Archives</h1>
	<ul>
		<?php wp_get_archives('type=monthly'); ?>
	</ul>
	</div> <!-- end 2col footer chunk -->

	<div class="left-03col">
	<h1>Meta</h1>
	<ul>
		<?php wp_register(); ?>
		<li><?php wp_loginout(); ?></li>
		<li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
		<?php wp_meta(); ?>
	</ul>
	</div> <!-- end 2col footer chunk -->

	<div class="left-03col">
	<h1>Subscribe</h1>
	<ul>
		<li><a href="<?php bloginfo('rss2_url'); ?>">Entries (RSS)</a></li>
		<li><a href="<?php bloginfo('comments_rss2_url'); ?>">Comments (RSS)</a></li>
	</ul>
	</div> <!-- end 2col footer chunk -->

	<?php endif; ?>

	<div class="right-03col">
	<h1>Also</h1>
	<ul>
		<li>Powered by <a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
		<li>Using the <a href="http://www.snoother.com/extricate/" title="Using the Extricate theme, a minimalist, grid-based theme for bloggers">Extricate</a> theme</li>
	</ul>
	</div> <!-- end 2col footer chunk -->

	</footer><!-- end colophon -->
</div><!-- end page -->

<?php wp_footer(); ?>

</body>
</html>