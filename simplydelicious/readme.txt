Thanks for downloading the 'Simply Delicious' v1.2 Wordpress Theme!

All documentation, credits and support info can be found at:
http://mmminimal.com/wordpress-theme/simply-delicious/documentation/

Cheers,
ThemeCobra Team

http://themecobra.com
http://twitter.com/themecobra