<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
 /*
*/
 
 get_header(); 

?>

<?
global $options;
foreach ($options as $value) {
    if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); }
}
?>
<?php //echo $mmcp_box1;  ?>


<?php  if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
		
		
	<div id="sub-posts">

			<div class="subtitles">
			<?php  $id = get_post_thumbnail_id($id); ?>
							
			<div id="featuredthumb-small" style="background:url(<?php $myimage = 
			wp_get_attachment_image_src($id, full); echo reset($myimage); ?>) top no-repeat;">
			</div>
                
               		<h4><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h4>
			<h3 class="date-title"><?php the_time('M j, Y @ G:i') ?> in <?php the_category(', ') ?></h3><?php the_content_limit(150, "Read more➝"); ?>
    
			
			
			</div>

			
</div>
			<div class="span-4" id="rightsidebar">
				<?php get_sidebar(); ?>
			</div>
<?php endwhile; ?>
			<?php endif; ?>
	
</div>
</div>
</div>
</div>
		
	      <?php get_footer(); ?>
