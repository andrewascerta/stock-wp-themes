<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>

	
<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar')) : ?>
	
		<div class="textwidget">
			<ul>
			<h3 class="widget-title">Search</h3>
			<li id="search" class="widget-container widget_search">
				
				<form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
		<div><input type="text" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" />
		<input id="searchsubmit" type="image" src="<?php bloginfo('template_directory'); ?>/images/search.gif" alt="Submit" />
			</li>
			</ul>
		</div>
		<div class="textwidget">
			
			<li id="archives" class="widget-container">
				<h3 class="widget-title"><?php _e( 'Archives', 'twentyten' ); ?></h3>
				<ul>
					<?php wp_get_archives( 'type=monthly' ); ?>
				</ul>
			</li>
			
		</div>
		
		<div class="textwidget">
			
			<li id="meta" class="widget-container">
				<h3 class="widget-title"><?php _e( 'Meta', 'twentyten' ); ?></h3>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</li>
		</div>

<?php endif; ?>
		
