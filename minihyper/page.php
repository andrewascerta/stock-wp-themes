<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
 /*
*/
 
 get_header(); 

?>
		
<?php  if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		
		<?php  $id1 = get_post_thumbnail_id($id); ?>
			
				
<div id="featuredthumb" style="background:url(<?php $myimage = wp_get_attachment_image_src($id1, full); echo reset($myimage); ?>) center no-repeat;">
		</div>


<div class="span-13" >
		
		
	<div class="span-13 last">


		
		<div id="maintitle" >
<div class="span-16">
<h1><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?>	</a></h1>
</div>
<h3 class="date-title">
<?php the_time('M j, Y @ G:i') ?> 
</h3>
</div>
				
				
			<div class="span-13 last"  id="maintext" > 

			
			<?php the_content(); ?>
			
				
			</div>
		</div>
</div>
<div class="span-4" id="rightsidebar">
<?php include('sidebar.php'); ?>
</div>				
		
					
			
</div>
</div>    
			
			
			<!--/box -->

		<?php endwhile; ?>

	<?php endif; ?>
	


</div>




		
	      <?php get_footer(); ?>
