<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
 /*
Template Name:HOME
*/
 
 get_header(); 

?>

<?
global $options;
foreach ($options as $value) {
    if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); }
}
?>
<?php //echo $mmcp_box1;  ?>
		


<div style="margin:0;" class="span-21">
<?php $my_query = new WP_Query('posts_per_page=1');
  while ($my_query->have_posts()) : $my_query->the_post();
  $do_not_duplicate = $post->ID;?>

		
	<div class="span-21 last" id="main-post">

			<?php  $id = get_post_thumbnail_id($id); ?>
							
			<div id="featuredthumb" style="background:url(<?php $myimage = wp_get_attachment_image_src($id, 
			full); echo reset($myimage); ?>) top no-repeat;">
			</div>

	
			<div id="maintitle" >

				<div class="span-21">
					<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to 
					<?php 
					the_title_attribute(); ?>"><?php the_title(); ?>	</a></h2>
				</div>
				<h3 class="date-title">
				<?php the_time('M j, Y @ G:i') ?> in <?php the_category(', ') ?>
				</h3>
			</div>
					
				
			<div style="border-bottom: 1px dotted #000000;" class="span-21 last" > 
	
				<div id="container"> 
	
				<?php the_content_limit(800,'<strong>Read more ➝</strong>'); ?>
	 			<div id="top-widget">
					
					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Top Widget')) : ?>
					
					<?php endif; ?>

				</div>
				</div> 
				
			</div>

				
	</div>					
    

	<?php endwhile; ?>

	<div id="sub-posts">
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); 
		if( $post->ID == $do_not_duplicate ) continue; ?>	
	
		<?php  $id = get_post_thumbnail_id($id); ?>
    
       		<div class="subtitles">
			
							
			<div id="featuredthumb-small" style="background:url(<?php $myimage = 
			wp_get_attachment_image_src($id, full); echo reset($myimage); ?>) top no-repeat;">
			</div>
                
               		<h4><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h4>
			<h3 class="date-title"><?php the_time('M j, Y @ G:i') ?> in <?php the_category(', ') ?></h3>

                	<?php the_content_limit(150, '<strong>Read more➝</strong>'); ?>
                
               		
            
            	</div>
         
        

	<?php endwhile; ?>
	<?php endif; ?>


	
</div>

<div class="span-4" id="rightsidebar">
<?php include('sidebar.php'); ?>
</div>
<div class="span-13 last">
			<div style="float:left;font-weight: bold;">
<?php previous_post();?></div>   <div style="float:right;font-weight: bold;"><?php next_post();?> </div>
			</div>

</div>



		
	      <?php get_footer(); ?>
