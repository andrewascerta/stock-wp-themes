<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
 /*
*/
 
 get_header(); 

?>
		
<?php  if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		
		<?php  $id1 = get_post_thumbnail_id($id); ?>
			
				
<div id="featuredthumb" style="background:url(<?php $myimage = wp_get_attachment_image_src($id1, full); echo reset($myimage); ?>) center no-repeat;">
		</div>


<div style="float:left;" class="span-13" >
		
		
	


		
		<div id="maintitle" >
<div class="span-16">
<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?>	</a></h2>
</div>
<h3 class="date-title">
<?php the_time('M j, Y @ G:i') ?> in <?php the_category(', ') ?>
</h3>
</div>
			
				
				
			<div id="maintext" > 

			<p> 
			<?php the_content(); ?>
			</p> 
				
			</div>
<div class="span-13" id="postfoot">			
	<?php comments_template(); ?>	
</div>			
</div>



<div class="span-4" id="rightsidebar">
<?php include('sidebar.php'); ?>
</div>				
		  
				
 			
</div>

</div>
			   
			
			
			<!--/box -->

		<?php endwhile; ?>

	<?php endif; ?>
	


</div>




		
	      <?php get_footer(); ?>
